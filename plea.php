<?php
if (file_exists('vault/ip.txt'))
{
	$ip = unserialize(file_get_contents('vault/ip.txt'));
}
else $ip = [];
if (isset($ip[$_SERVER['REMOTE_ADDR']]))
{
	sleep(1);
	die("You already appealed once.");
}
$ip[$_SERVER['REMOTE_ADDR']] = date('r');

file_put_contents('vault/plea.log',date('Y-m-d H:i:s')."\n".serialize($_POST)."\nIP: ".$_SERVER['REMOTE_ADDR']."\n\n",FILE_APPEND);
file_put_contents('vault/ip.txt',serialize($ip));

echo 'Your appeal has been noted, thank you.';
