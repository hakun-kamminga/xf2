<?php
/**
 * Fetches a random set of images from imgur.
 * Within this set, we add page numbering and a way to set the amount 
 * of images displayed on a page.
 */
session_start();
require dirname (__FILE__) . '/src/imgur.php';

$imgur = new Imgur();

$page  = empty($_REQUEST['page'])  ? (empty($_REQUEST['last_page']) ? 1 : $_REQUEST['last_page']) : intval($_REQUEST['page']);
$imgpp = empty($_REQUEST['imgpp']) ? 10 : intval($_REQUEST['imgpp']);

/** 
 * imgur retrieves 60 images at a time, building fast against their rate limiter. (no idea why it must be 60) - Can't circumvent this - perhaps with special headers?
 * 
 * Keep these in session to allow for pagination unless a reshuffle is required,
 * so that hapless users can peruse their results without strange (read: unintended) artifacts.
 * 
 * Now, I get that perhaps random should be random, but since our glorious masters 
 * throw a fixed [randomised] set at us, this pagination should apply only to their initial barrage / request.
 * 
 * Theirs is their bandwidth to squander: I shall only concern myself with the facts and not their (imgur) weird idiosyncracies or inefficiancies.
 
 * I know 1 is hard coded - was not expecting a barrage of multipliers of (not even sure what; their endpoint just throws up a revolting amplitude of data) 
 * their API: They locked me out for an hour when I assumed a criteria (albeit random) was a thing because I had no idea their response. 
 * 
 * Needs discussion. Not happy. Based it down, I am still breating and will construct this logic to yield to their Mordorbidian extrospective.
 * 
 * Below is a meagre view; did my best to shield it from any logic. Templatng engines welcome but for now it's hit and miss.
 * Fun fact: The MVVC (Model-View-ViewcModel) makes for a great tongue-twister when injecting a tertiary layer of logic that arguably yields 
 * greater scalabilty at the expense of sanity.
*/

if (empty($_SESSION['imgur_response']) || !empty($_REQUEST['reshuffle'])) { // Initial request.

	/**
	 * To limit request from API. If this is not set, page numbering won't make sense because the set of random images will
	 * have changed for every response.
	 * 
	 * When a new set is required, it listenes out for reshuffle param.
	 */ 
	$_SESSION['imgur_response'] = $imgur->getImages();
	
}
?><html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<title>Michael Paul Imgur</title>
		<style>
			.imgur_image{
				height:100px;
				width:auto;
				margin:2px;
			}
		</style>
	</head>
	<body>
		<div class="container">
				<h1>Images</h1>
				<?php
				$pagination = '';
				$pagenumber = 0;
				$pagestotal = ceil(count($_SESSION['imgur_response']) / $imgpp);
		
				if (is_array($_SESSION['imgur_response']) && sizeof($_SESSION['imgur_response']) < 1000) {	// Never trust an API regardless their reputation.
					
					for ($i = (($page - 1) * $imgpp); $i < ceil($page * $imgpp); $i++) {
					
						$result = $_SESSION['imgur_response'][$i];	// I know this is wrong, but without the time to implent a proper iterator it must do (mostly a feature of PHP7.4 anyway).
						
						if (!empty($result->images)) {	// No subset of images attached to this submission. Could do a recursive but for now, grab the first one.
						
							$src = $result->images[0]->link;
							
						} else {
							$src = $result->link;		// When there's no portfolio attached, imgur decides to make the ->link the main src: If not, the URL to their showcase.
						}
						?>
						<img class="imgur_image" src="<?= $src ?>" alt="<?= htmlentities($result->title) ?>">
						<?php
						
					}
				}
				?>
			<div class="pagenumbers">
			<form method="get">
			
			<div class="form-group"><?php
			
			for ($i = 1; $i <= $pagestotal; $i++) {
				$class = ($i == $page ? 'btn-primary' : 'btn-default');
				echo '<input type="submit" name="page" value="' . $i . '" type="submit" class="btn ' .  $class . '">';
			}
			
			?>
			</div>
			
			<div class="form-group">
			<label for="imgpp">How many results per page?</label>
			<input id="imgpp" name="imgpp" value="<?= $imgpp ?>" placeholder="How many results per page>">
			</div>
			<input type="hidden" name="last_page" value="<?= $page ?>">
			<input type="submit" value="Go" class="btn btn-success">
			<input type="submit" name="reshuffle" value="Reshuffle" class="btn btn-default">
		</form>
		</div>
	</body>
</html>
