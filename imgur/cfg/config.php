<?php
/**
 * Configuration file for use with imgur API and default settings
 */ 
return [
	'app_name'      => 'Michael Paul Holidays',
	'client_id'     => 'd2c9b38ef3ca923',
	'client_secret' => 'beca26099e5caa1ca58b45a0157ceca10721ca04',
	'api_endpoint'  => 'https://api.imgur.com/3/',
	'max_images'    => 20,
	'default_maxpp' => 10,	// default amount of images to display per page.
	];
	