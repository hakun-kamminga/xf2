<?php

#namespace \tengreenbottles\models;

class Objects
{
	const FORMATTED_NUMBERS = [
		0  => 'no',
		1  => 'one',
		2  => 'two',
		3  => 'three',
		4  => 'four',
		5  => 'five',
		6  => 'six',
		7  => 'seven',
		8  => 'eight',
		9  => 'nine',
		10 => 'ten',
		11 => 'eleven',
		12 => 'twelve',
		13 => 'thirteen',
		14 => 'fourteen',
		15 => 'fifteen',
		16 => 'sixteen',
		17 => 'seventeen',
		18 => 'eighteen',
		19 => 'nineteen',
		20 => 'twenty',
	];
	
	public function __construct()
	{
		
	}
	/**
	 * Make computer say numbers like a human would.
	 * To a certain limit, would love to make a decent quintillion parser but that is not for today.
	 * 
	 * @param int
	 * @return string
	 */ 
	public static function Howmany( int $number ) : string
	{
		return self::FORMATTED_NUMBERS[$number] ?? self::FORMATTED_NUMBERS[10];
	}
}