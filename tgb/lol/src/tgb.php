<?php

require dirname( __FILE__ ) . '/../models/bottles.php';	// yeah, we could call it objects.php but let's stick with the spirit of this exercise.

class Tgb {
	
	const MAX_OBJECTS = 20;
	
	protected $objects;	// Protected in case we wish to build on this awesome Tgb
	protected $lyrics;	
	
	public function __construct()
	{
		$this->lyrics  = file_get_contents( dirname(__FILE__) . '/../data/lyrics.txt' );
	}
	/**
	 * Public setter
	 * 
	 * @param int $bottles
	 * @return void
	 */ 
	 
	public function setObjects(int $objects = 10)
	{
		if ($objects > self::MAX_OBJECTS) {	// Failsafe in case we ask for more bottles than we've algorhithmed for.
			$objects = self::MAX_OBJECTS;
		}
		if ($objects <= 1) {				// No bottles means nothing can happen, so make it the least usable qty.
			$this->objects = 1;
		} else {
			$this->objects = $objects;
		}
	}
	/**
	 * Public getter
	 * 
	 * @return int
	 */ 
	 
	public function getObjects() : int
	{
		return $this->objects;
	}
	
	/**
	 * Get the actual result.
	 * @param void
	 */
	 
	public function generateLyrics() : string
	{
		return $this->Parse($this->lyrics); 
	}
	
	/**
	 * Public setter
	 * 
	 * @param int $bottles
	 * @return void
	 */ 
	public function setColor($color = '#000')
	{
		$this->color = $color;
	}
	
	/**
	 * Replace numbers with human readable text using the model we created
	 * to manage the transformation of this data and any other business logic.
	 * 
	 * @param string $lyrics
	 * @return string
	 */
	 
	public function Parse($lyrics)
	{
		// In a decently run universe, we'd use a dedicated parser but for now this will suffice.
		// More than anything because to parse things, you'll need to create a digital mapping interface
		// which is not too complicated (think of keys matching values) but it's beyond the scope of what we're doing here.
		
		$return = str_replace('{{objects}}', Bottles::Howmany($this->objects), $lyrics);				// Let the model decide how to handle bounds and limits.
		$return = str_replace('{{remaining-objects}}', Bottles::Howmany(--$this->objects), $return);	// Parse the other template variable.
		return $return;
	}
}