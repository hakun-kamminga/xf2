<?php

require dirname( __FILE__ ) . '/../models/objects.php';	

class Tgb {
	
	const MAX_OBJECTS = 20;
	/** @var int $numbers **/
	protected $numbers;	
	/** @var string $lyrics **/
	protected $lyrics;	
	/** @var string $color **/
	private   $color;
	/** @var string $objectName **/
	private   $objectName;
	
	public function __construct()
	{
		$this->lyrics = file_get_contents( dirname(__FILE__) . '/../data/lyrics.txt' );
		$this->color  = 'green';
		$this->objectName = 'bottles';
	}
	/**
	 * Public setter
	 * 
	 * @param int $bottles
	 * @return void
	 */ 
	 
	public function setObjects(int $numbers = 10, string $color = 'green', string $objectName = 'bottles')
	{
		$this->color      = $color;
		$this->objectName = $objectName;
		
		if ($numbers > self::MAX_OBJECTS) {	// Failsafe in case we ask for more bottles than we've algorhithmed for.
			$numbers = self::MAX_OBJECTS;
		}
		if ($numbers <= 1) {				// No bottles means nothing can happen, so make it the least usable qty.
			$this->numbers = 1;
		} else {
			$this->numbers = $numbers;
		}
	}
	/**
	 * Public getter
	 * 
	 * @return int
	 */ 
	 
	public function getnumbers() : int
	{
		return $this->numbers;
	}
	
	/**
	 * Get the actual result.
	 * @param void
	 */
	 
	public function generateLyrics() : string
	{
		return $this->parse($this->lyrics); 
	}
	
	/**
	 * Public setter
	 * 
	 * @param int $bottles
	 * @return void
	 */ 
	public function setColor($color = 'green')
	{
		$this->color = $color;
	}
	
	/**
	 * Public setter
	 * 
	 * @param int $bottles
	 * @return void
	 */ 
	public function setObjectName($name = 'bottles')
	{
		$this->objectName = $name;
	}
	
	/**
	 * Replace numbers with human readable text using the model we created
	 * to manage the transformation of this data and any other business logic.
	 * 
	 * @param string $lyrics
	 * @return string
	 */
	 
	public function parse($lyrics)
	{
		// In a decently run universe, we'd use a dedicated parser or templating engine like twig but for now this will suffice.
		// More than anything because to parse things, you'll need to create a digital mapping interface
		// which is not too complicated (think of keys matching values) but it's beyond the scope of what we're doing here.
		
		$return = str_replace('{{number}}', Objects::howMany($this->numbers), $lyrics);				// Let the model handle transformations
		$return = str_replace('{{uc-number}}', ucfirst(Objects::howMany($this->numbers)), $lyrics);	
		$return = str_replace('{{remaining-number}}', Objects::howMany(--$this->numbers), $return);	// Parse the other template variable.
		$return = str_replace('{{color}}', $this->color, $return);
		$return = str_replace('{{objects}}', $this->objectName, $return);
		$return = str_replace('{{object}}', Objects::makeSingular($this->objectName), $return);
		return $return;
	}
}