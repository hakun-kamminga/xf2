<?php

#namespace \tengreenbottles\models;

class Objects
{
	private $properties = [];
	
	const FORMATTED_NUMBERS = [
		0  => 'no',
		1  => 'one',
		2  => 'two',
		3  => 'three',
		4  => 'four',
		5  => 'five',
		6  => 'six',
		7  => 'seven',
		8  => 'eight',
		9  => 'nine',
		10 => 'ten',
		11 => 'eleven',
		12 => 'twelve',
		13 => 'thirteen',
		14 => 'fourteen',
		15 => 'fifteen',
		16 => 'sixteen',
		17 => 'seventeen',
		18 => 'eighteen',
		19 => 'nineteen',
		20 => 'twenty',
	];
	
	public function __construct()
	{
		
	}
	/**
	 * Set a property
	 * 
	 * @param mixed $key
	 * @param mixed $val
	 */ 
	public function setProperty($key, $val)
	{
		$this->properties[$key] = $val;
	}
	
	/**
	 * Get a property
	 * 
	 * @param mixed $key
	 * @return mixed
	 */ 
	public function getProperty($key)
	{
		return $this->properties[$key];
	}
	
	/**
	 * Make computer say numbers like a human would.
	 * To a certain limit, would love to make a decent quintillion parser but that is not for today.
	 * 
	 * @param int
	 * @return string
	 */ 
	public static function howMany( int $number ) : string
	{
		return self::FORMATTED_NUMBERS[$number] ?? self::FORMATTED_NUMBERS[10];
	}
	
	/**
	 * Make computer use grammar like a human would.
	 * 
	 * @param string
	 * @return string
	 */ 
	public static function makeSingular($pluralNoun)
	{
		$noun = strtolower($pluralNoun);
		
		// E.g. ferries, cranberries, enemies
		
		if (substr($noun, -3) === 'ies') {
			return substr($noun, 0, -3) . 'y';
		} else {
			// E.g. shoelaces, bottles, ramakins, snakes
			return substr($noun, 0, -1);
		}
	}
}