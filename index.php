<?php
/**
 * Load the XCMS monolithic core
 * It acts like a bootloader.
*/
session_start();

define('WEBROOT',dirname(__FILE__));

require_once(dirname(__FILE__)."/mvc/cfg/web.php");
require_once(dirname(__FILE__)."/mvc/components/db.php");
require_once(dirname(__FILE__)."/mvc/core.php");

//require WEBROOT.'/zbblock_0_4_10a3/zbblock.php';

$X2 = new Core();
$X2-> Render(); 
