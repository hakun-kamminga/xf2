// Form JS
$('#add-form').click(function(){
	$('#new-form-title').html('New Form');
	$('#create-button').html('Create');
	$('#form-title').val('');
	$('#form-description').val('');
	$('#add_form').slideToggle();
	
});
function addForm(what){
	$('#form-id').val('');
	$.post('/ajax',$(what).serialize(),function(response){
		
		alert(response);
		
	});
	return false;
}
function edit(what){
	$('#new-form-title').html('Edit Form');
	$('#form-id').val($(what).data('form-id'));
	$('#create-button').html('Save changes');
	$.post('/ajax',{model:'form',action:'load',form_id: $(what).data('form-id')},function(response){
		$('#add_form').slideToggle();

		$('#form-title').val(response.form_title);
		$('#form-id').val(response.form_id);
		$('#form-description').val(response.form_description);

		console.log(response);
		
	});
}