$(document).ready(function(){
	$( function() {
		$( "#browser" ).sortable({
			stop: function(event,ui){
				
				console.log(ui);
				updateWeights(ui);
			}
		});
		$( "#browser" ).disableSelection();
	} );
});
function save(what){
	$('#save-btn').html('<span class="fa fa-spin fa-spinner"></span> Save');
	var markup = $('#editor').summernote('code');
	$('#page_content').val(markup);
	$.post( "/ajax", $(what).serialize(),function( data ) {
		$('#save-btn').html('<span class="fa fa-file"></span>&nbsp; Save');
	});	
	//console.log(markup);
	return false;
}

function activate(what){
	if ($(what).prop('checked') === true){
		alert('Activating '+$(what).attr('id'));
	}
	else alert('Closing '+$(what).attr('id'));
	
}
function updateWeights(what){
	var weight = 100;
	$('#browser>li').each(function(){
		var pid = $(this).data('id');
		weight+= 10;
		if (pid === '' || pid === null || pid === undefined) {
			//something
			var a = null;
		}
		else {
			$.post( "/ajax", {model:'page',app:'page',page_weight:weight,page_id:pid},function( data ) {
				console.log(data);
			});		
		}
	});
}