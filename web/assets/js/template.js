function addRow(cols) {
	var rid = Math.floor(Math.random()*455767)+5;
	var out = '<div class="container" id="CNT'+rid+'"><div class="row grid-row-item" id="R'+rid+'">';
	    out+= '<span id="rowbtn'+rid+'" style="display:none" onclick="deleteRow(\'R'+rid+'\')" class="fa fa-times-circle red delete-row"></span>';
	    
	for(var i in cols)
	{
		console.log(cols[i]);
		out+= '<div id="C'+rid+'-'+i+'" onclick="display(this,\'col\',\''+rid+'\')" class="col-lg-'+cols[i]+' grid-col-item">';
		out+= '&nbsp;</div>';
	}
	out+= '</div></div>';
	$('#viewport').append(out);
}

function display(what,type,rid) {
	if (rid.substr(0,1) == 'R') {
		rid = rid.replace('R','');
	}
	if (type === 'col') {
		
		$('#col-id').val(what.id);
		$('#col-class').val(what.className);
		
		if (rid!=0)$('#row-id').val('R'+rid);
		if (rid!=0)$('#row-class').val($('#R'+rid).attr('class'));
		
		$('.delete-row').hide();
		$('#rowbtn'+rid).show();
	}
	
}
function deleteRow(what) {
	$('#'+what).remove();
}
function reduceCol() {
	var cid = $('#col-id').val();
	var cls = document.getElementById(cid).className;
	var ind = cls.split(' ');
	
	for(var i in ind) {
		
		if (ind[i].substr(0,7) == 'col-lg-'){
			var num = ind[i].substr(-2);
			if (num.substr(0,1)=='-') {
				num = ind[i].substr(-1);
			}		
			num = parseInt(num);
			if(num>1) {
				
				$('#'+cid).removeClass('col-lg-'+num);
				num--;
				$('#'+cid).addClass('col-lg-'+num);
				display(document.getElementById(cid),'col',0);
			}
		}		
	}
}
function increaseCol() {
	var cid = $('#col-id').val();
	var cls = document.getElementById(cid).className;
	var ind = cls.split(' ');
	
	for(var i in ind) {
		
		if (ind[i].substr(0,7) == 'col-lg-'){
			var num = ind[i].substr(-2);
			if (num.substr(0,1)=='-') {
				num = ind[i].substr(-1);
			}
			num = parseInt(num);
			if(num<12) {
				
				$('#'+cid).removeClass('col-lg-'+num);
				num++;
				$('#'+cid).addClass('col-lg-'+num);
				display(document.getElementById(cid),'col',0);
			}
		}		
	}
}
function toggleFluid(what) {
	if($('#row-id').val() == '')return false;
	var rid = $('#row-id').val();
	rid = rid.replace('R','');
	$('#CNT'+rid).removeClass('container').removeClass('container-fluid');
	if (what.checked){
		$('#CNT'+rid).addClass('container-fluid');
	}
	else $('#CNT'+rid).addClass('container');
	display('#'+$('#col-id').val(),'col',rid);
	return true;
}
function updateText(what){
	var o = document.getElementById($('#col-id').val());
	o.innerHTML = what;
	return true;
}
function addMedia(what){
	var o = document.getElementById($('#col-id').val());
	$(o).append('<div class="summernote"><img src="'+what.src+'" class="image-generic"><p>Edit text...</p></div>');
	$('.summernote').summernote({
		airMode: true
	});
	return true;
}
function makeEditable(){
	var o = document.getElementById($('#col-id').val());
	$(o).append('<div class="summernote">'+o.innerHTML+'</div>');
	$('.summernote').summernote();
	return true;
}
function makeStatic(){
	var o = document.getElementById($('#col-id').val());
	var markupStr = $('#'+o.id+' .summernote').summernote('code');
	$('#'+o.id+' .summernote').remove();
	$(o).html(markupStr);
	return true;
}
function deleteCol(){
	var o = document.getElementById($('#col-id').val());
	$(o).remove();
	return true;
}