$(document).ready(function(){
	
	/**
	 * Do all these things on every page in backend.
	*/
	$('.timer').each(function(){
		
		$(this).html('4 days, 36 hours');
		
	});
	
})
$('.navicon').hover(function(){
	console.log($(this).attr('title'));
	$('#nav-toolbar').html($(this).attr('title'));
}, function(){
	
	$('#nav-toolbar').html('');
	
});
function saveSettings(what){
	$('#save-btn').html('<span class="fa fa-spin fa-spinner"></span> Save');
	$.post( "/ajax", $(what).serialize(),function( data ) {
		$('#save-btn').html('<span class="fa fa-file"></span>&nbsp; Save');
		console.log(data);
	});	
	return false;
}
function newSetting(what){
	$('#new-btn').html('<span class="fa fa-spin fa-spinner"></span> New');
	$.post( "/ajax", $(what).serialize(),function( data ) {
		$('#new-btn').html('<span class="fa fa-plus"></span>&nbsp; New');
		console.log(data);
		$('#basicModal').modal('toggle');
	});	
	return false;
}
function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}