<?php 
if(!defined('WEBROOT'))
{
	header('Location:/');
	exit();
}
$SiteController = new SiteController();

?><!DOCTYPE html>
<html lang="en">
  <head><!-- Frontend. Default Theme -->
    <meta charset="utf-8">
    <meta name="generator" content="<?=X2_VERSION ?>">
    <title><?=htmlentities($SiteController->attributes['page_title']) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/themes/default/assets/css/main.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /><link href="/fa/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="/themes/default/assets/css/dodecahedron.css" type="text/css">
	<style>
	.navbar-brand>img{
	    margin-top: -13px;
	    max-height: 50px;
	    max-width: 80px;
	}.marginBottom-0 {margin-bottom:0;}
	
	.dropdown-submenu{position:relative;}
	.dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
	.dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
	.dropdown-submenu:hover>a:after{border-left-color:#555;}
	.dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
	</style>
	<script src='https://www.google.com/recaptcha/api.js'></script>
  </head>
<body>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?=\Utils::GetConfig('FB App Id')['setting_value'] ?>',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.10'
    });
    FB.AppEvents.logPageView();   
  };

	function checkLoginState() {
	  FB.getLoginStatus(function(response) {
	  	console.log(response);
	  	if (response.status === 'connected')
	  	{
	  		
	  	}
	  	alert('Status: '+response.status);
	    statusChangeCallback(response);
	  });
	}   
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));   
</script>

<!--
<fb:login-button 
  scope="public_profile,email"
  onlogin="checkLoginState();">
</fb:login-button>
-->

<div class="bs-component">
<?php echo $SiteController::Nav(); 


require_once WEBROOT.'/mvc/components/bootstrap.php';
$boot = new Bootstrap();
$boot-> Parse();


?>
</div>
<div class="container-fluid" style="height:200px;width:100%;background-image:url(<?=$SiteController->attributes['page_header_images'] ?>);background-size:cover">
		<div class="container">
			
		<h1><?php echo htmlentities($SiteController->attributes['page_title']); ?></h1>
		</div>

</div>
<div class="container">
 

<div class="view">
  <div class="plane main">
    <div class="circle"></div>
    <div class="circle"></div>
    <div class="circle"></div>
    <div class="circle"></div>
    <div class="circle"></div>
    <div class="circle"></div>
  </div>
</div>



 <div class="page_body"><?php echo Utils::SafeHtml($SiteController::Page()['page_content']); 
 echo("<pre style='display:none'>").print_r(Utils::ParseShortcodes('[robot name="optimus" mega="tron"]'),true)."</pre>"; ?>
 
 </div>
</div>
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script>$(document).ready(function(){
  $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
    event.preventDefault(); 
    event.stopPropagation(); 
    $(this).parent().siblings().removeClass('open');
    $(this).parent().toggleClass('open');
  });
});
</script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<?php if (isset($endbody))echo $endbody; ?>
</body>
</html>
