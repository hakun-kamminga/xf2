<?php
/**
 * This is the XCMS2 Core. It acts like a bootloader for everything else.
*/
ini_set('display_errors',true);
require "components/utils.php";			// Remember ol' toolshed
require "components/controller.php";	// Base that every page extends from.

class Core
{
	private $plugin_manifest;			// For later. Scan plugins in folder but compare to account.
	private $cache_manager;				// Just to be fancy in case we have several one day.
	private $content;					// Stuff to flush into the page, usually a page loaded from DB.
	private $layout	 = 'frontend';		// Default layout. Assume we're displaying a web page.
	private $request = '';				// E.g. /admin=sysconfig
	private $slug    = '';				// Slug: e.g. /dancing-with-smurfs
	public  $controller;				// Machine name of controller, e.g. "displaycontroller".
	
	public $route = [];
	
	public function __construct()
	{
		self::StartSession();
		$this->ResolveRoute();
	}
	private static function StartSession()
	{
		$_SESSION['remote_ip'] = $_SERVER['REMOTE_ADDR'];
		/**
		 * We are all guests for now unless a status is already set.
		*/
		if (empty($_SESSION['priv']))
		{
			$_SESSION['priv'] = $_SERVER['REMOTE_ADDR'];
		}
		if (!isset($_SESSION['backend_theme']))
		{
			$_SESSION['backend_theme'] = [];
			/**
			 * Load backend theme from DB.
			*/
			$theme = self::GetConfig("Backend Theme");
			if (empty($theme))
			{
				$_SESSION['backend_theme']['css'] = 'solar';
			}
			else $_SESSION['backend_theme']['css'] = $theme;
		}
		/**
		 * Invoke base Controller.
		 * Auth() is automatically performed without implicitly calling it, in case you are wondering.
		 * This way it is impossible to accidentally create pages or interfaces that don't have any sort
		 * of access control.
		*/
		$a = new X2Controller();
	}
	private function ResolveRoute()
	{
		/**
		 * HK: Will need to invoke Component for this:
		 * 1. URL parser that makes sure requests are properly routed
		 * 2. Everything else
		*/ 
		if (!empty($_POST['ajax']))
		{
			switch($_POST['ajax'])
			{
				case "saveimage":
					print_r($_FILES);
					die();
				break;
					
			}
		}
		if (!empty($_GET['r']))
		{
			$this->route = explode("/",$_GET['r']);
		}
		if (!empty($_GET['q']))
		{
			switch($_GET['q'])
			{
				case "admin":
				if (empty($_SESSION['pin']) && ($_SESSION['priv'] != 'admin' && $_SESSION['priv'] != 'root'))
				{
					if (!isset($_SESSION['tries']))$_SESSION['tries'] = 5;
					if (empty($_POST['pin']))
					{
						
						header('Location:/authenticate');
						exit();
						
						$header  = 'Please enter your PIN';
						$title   = 'Authentication Required';
						
						$content = file_get_contents(WEBROOT.'/mvc/views/layouts/pin.php');
						require_once WEBROOT.'/mvc/views/layouts/single_operation.php';
						exit();
					}
					$pin = \Utils::GetConfig('PIN')['setting_value'];
					if ($_POST['pin'] != $pin)
					{
						$_SESSION['tries']--;
						
						if ($_SESSION['tries']>0)
						{
							$header  = 'WRONG PIN YOU IDIOT ('.$_SESSION['tries'].') tries remaining.';
							$title   = 'Wrong Password!';
							$content = file_get_contents(WEBROOT.'/mvc/views/layouts/pin.php');
						}
						else
						{
							$title   = 'Lockout';
							$content = file_get_contents(WEBROOT.'/mvc/views/layouts/401.php');
						}
						
						require_once WEBROOT.'/mvc/views/layouts/single_operation.php';
						exit();						
					}
					if ($_SESSION['tries']>0)
					{
						$_SESSION['priv'] = 'root';
						$_SESSION['user'] = [

							'id' => 100,
						];
					}
					else
					{
						// Add something to firewall here.
						header("Location:/");
						exit();
					}
					
				}
				if (empty($this->route[2]))
				{
					$this->route[2] = 'dashboard';	//
				}
				if (file_exists(dirname(__FILE__)."/modules/backend/".\Utils::MachineName($this->route[2]).".php"))
				{
					$this->layout  = 'backend';
					$this->request = dirname(__FILE__)."/modules/backend/".\Utils::MachineName($this->route[2]).".php";
					/**
					 * Invoke Controller.
					*/
					require(dirname(__FILE__)."/controllers/".\Utils::MachineName($this->route[2])."Controller.php");
				}
				else \Utils::Exception(["message"=>"No such admin area: <cite>".\Utils::MachineName($this->route[2])."</cite>","debug" => "Unit:\nController\n"]);					
				break;
				case "authenticate":
				$this->layout = 'backend';
				$header  = '';
				$title   = 'Authentication Required';
				
				$content = file_get_contents(WEBROOT.'/mvc/views/layouts/login.php');
						
				require_once WEBROOT.'/mvc/views/layouts/single_operation.php';
				exit();
				break;
				case "login":
				/**
				 * Autoload required libraries for social login
				*/
				require_once WEBROOT.'/vendor/autoload.php';
				/**
				 * Let siteController do the display
				*/
				require_once WEBROOT."/mvc/controllers/siteController.php";
				$site = new SiteController();
				$site->LoginFB();				// Using FB for now.
				break;
				case "auth":
				require_once WEBROOT.'/vendor/autoload.php';
				require_once WEBROOT."/mvc/controllers/siteController.php";
				$site = new SiteController();
				$site->AuthenticateFB();		// Using FB for now.
				break;
				case "logout":
				session_destroy();
				header("Location:/");
				exit();
				break;
				case "ajax":
				/**
				 * Invoke Model.
				 * To make our lives easier by way of automation, it will always expect an object of $Model.
				 * 
				 * All models contain a Process method, that will iterate throught the $_POST and modify as needed.
				 * This way, we prevent Core from becoming an endless switch() statement.
				 * 
				 * Further, to enforce coding standards, every Controller MUST have a model, even if it's an empty file.
				 * This will also simplify wizards that generate Models, Views and Controllers by reducing evaluations 
				 * and offer a range of other benefits.
				 * 
				*/
				if (!empty($_POST['model']))
				{
					require dirname(__FILE__)."/models/".\Utils::MachineName($_POST['model'])."Model.php";
					$Model->Process($_POST);
				}	
				require WEBROOT.'/mvc/components/ajax.php'; 
				$a = new AjaxHandler();
				$a-> ScanRequirements();
				$a-> Parse();
				if (X2_VERBOSE)die('POST data received: '.json_encode($_POST));
				exit();
				break;
				default:
				require_once WEBROOT."/mvc/controllers/siteController.php";
				include WEBROOT.'/themes/default/template/frontend.php';
				exit();
				break;
			}
		}
		if (!empty($_POST['content']))
		{
			die(htmlentities($_POST['content']));
		}
		if (!empty($_REQUEST['theme']))
		{
			$_SESSION['backend_theme']['css'] = $_REQUEST['theme'];
		}
		if (!empty($_REQUEST['theme_navbar']))
		{
			if (empty($_SESSION['backend_theme']['navbar']))
				$_SESSION['backend_theme']['navbar'] = 1;
			else 
				$_SESSION['backend_theme']['navbar'] = '';
		}
	}
	public function Render()
	{
		if ($_SESSION['priv'] === 'admin' || $_SESSION['priv'] === 'root')
		{
			$content = '<h1>Default Page</h1>';
			if (!empty($this->request))
			{
				$content = file_get_contents($this->request);
			}
			$content.= '<script>var request = '.json_encode($_REQUEST).';</script>';
			require(WEBROOT."/mvc/controllers/siteController.php");
			if ($this->layout == 'backend')  include WEBROOT."/mvc/views/layouts/backend.php";
			if ($this->layout == 'frontend') include WEBROOT."/themes/default/template/frontend.php";
		}
		else
		{
			require_once WEBROOT.'/mvc/controllers/siteController.php';
			include WEBROOT."/themes/default/template/frontend.php";
		}
	}
	public static function GetConfig($what)
	{
		$db = new Database();
		$db->query("SELECT * FROM settings WHERE setting_name = :name");
		$db->bind(":name",$what);
		$val = $db->single();
		if (!empty($val['setting_value']))
		{
			if (!empty($val['setting_optgroup']))
			{
				$a = explode("|",$val['setting_value']);
				foreach($a as $row)
				{
					if (substr($row,0,1) == '*')return str_replace("*","",$row);
				}
				return null;
			}
			else return $val['setting_value'];
		}
		return null;
	}
}