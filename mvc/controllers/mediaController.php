<?php
$media = true;

class MediaController extends X2Controller
{
	private $index;
	public $meta;
	private $cache;
	private $start;
	
	public function __construct()
	{
		$this->start = microtime();
	}
	public function Scan($folder = WEBROOT,$allData = [])
	{
	    // set filenames invisible if you want
	    $invisibleFileNames = array(".", "..", ".htaccess", ".htpasswd");
	    // run through content of root directory
	    $dirContent = scandir($folder);
	    foreach($dirContent as $key => $content) 
	    {
	        // filter all files not accessible
	        $path = $folder.'/'.$content;
	        if(!in_array($content, $invisibleFileNames)) 
	        {
	            // if content is file & readable, add to array
	            if(is_file($path) && is_readable($path)) 
	            {
	                // save file name with path
	                $allData[] = str_replace(WEBROOT,'',$path);
	            // if content is a directory and readable, add path and name
	            }
	            elseif(is_dir($path) && is_readable($path)) 
	            {
	                // recursive callback to open new directory
	                $allData = self::Scan($path, $allData);
	            }
	        }
	    }
    	return $allData;
	}
	public function Optimise($folder)
	{
		$files = $this->Scan($folder);
		foreach($files as $file)
		{
			echo $file."<br>";
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$type  = finfo_file($finfo,WEBROOT.$file);
			if (substr($type,0,13) == 'application/x')
			{
				\Utils::EventLog('Illegal file type: '.$file,'Security');
				unlink(WEBROOT.$file);
			}
		}
	}
	public function GetMedia($filename)
	{
		$db = new Database();
		$db -> query("SELECT * FROM media WHERE media_filename = :filename");
		$db -> bind(':filename',$filename);
		header("Content-Type:application/json");
		return json_encode($db->single());
	}
	public function GetRecent($json=false){
		$db = new Database();
		$db-> query("SELECT * FROM media ORDER BY media_uploaded DESC LIMIT 10");
		if (!$json)return $db->resultset();
		header("Content-Type:application/json");
		return json_encode($db->resultset());
	}
	public function GetDocuments($json=false){
		$db = new Database();
		$db-> query("SELECT * FROM media WHERE (
		media_filename LIKE '%.doc' OR
		media_filename LIKE '%.pdf' OR
		media_filename LIKE '%.txt' OR
		media_filename LIKE '%.docx' OR
		media_filename LIKE '%.rtf')
		
		ORDER BY media_title");
		if (!$json)return $db->resultset();
		header("Content-Type:application/json");
		return json_encode($db->resultset());
	}
	public function GetVideos($json=false){
		$db = new Database();
		$db-> query("SELECT * FROM media WHERE (
		media_filename LIKE '%.mkv' OR
		media_filename LIKE '%.avi' OR
		media_filename LIKE '%.mp4' OR
		media_filename LIKE '%.mov' OR
		media_filename LIKE '%.flv' OR
		media_filename LIKE '%.wmv')
		
		ORDER BY media_title");
		if (!$json)return $db->resultset();
		header("Content-Type:application/json");
		return json_encode($db->resultset());
	}
	public function GetMusic($json=false){
		$db = new Database();
		$db-> query("SELECT * FROM media WHERE (
		media_filename LIKE '%.mp3' OR
		media_filename LIKE '%.ogg' OR
		media_filename LIKE '%.wma' OR
		media_filename LIKE '%.aiff' OR
		media_filename LIKE '%.wav' OR
		media_filename LIKE '%.flac')
		
		ORDER BY media_title");
		if (!$json)return $db->resultset();
		header("Content-Type:application/json");
		return json_encode($db->resultset());
	}
	public function GetImages($json=false){
		$db = new Database();
		$db-> query("SELECT * FROM media WHERE (
		media_filename LIKE '%.jpg' OR
		media_filename LIKE '%.jpeg' OR
		media_filename LIKE '%.gif' OR
		media_filename LIKE '%.png' OR
		media_filename LIKE '%.tiff' OR
		media_filename LIKE '%.bmp')
		
		ORDER BY media_title");
		if (!$json)return $db->resultset();
		header("Content-Type:application/json");
		return json_encode($db->resultset());
	}
	public function MakeMicroThumbnail($what,$newfile)
	{
		require WEBROOT.'/vendor/simpleimage/simpleimage.class.php';
		$i = new SimpleImage($what);
		$i->square(32);
		$i->save($newfile);
	}
	public function MakeThumbnail($what,$newfile,$width,$height)
	{
		require WEBROOT.'/vendor/simpleimage/simpleimage.class.php';
		$i = new SimpleImage($what);
		$i->resize($width,$height);
		$i->save($newfile);
	}
	public function List(array $what)
	{
		$out = '<div class="ruow"><ul class="list-group">';
		foreach($what as $row=>$col)
		{
			$md5  = md5($col['media_filename']);	
			$size = \Utils::ToMB($col['media_filesize']);
			$del  = '<span class="fa fa-fw fa-close red floatright" onclick="if(confirm(\'Are you sure?\'))deleteMedia(\''.$md5.'\',\''.$col['media_filename'].'\')"></span>';

			if (empty($col['media_title']))
			{
				$col['media_title'] = \Utils::EndExplode('/',$col['media_filename']);
			}
			$out.= '<li class="list-group-item" data-filesize="'.$size.'" id="'.$md5.'">';
			$out.= '<div class="row"><div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">';
			$ext = \Utils::EndExplode('.',$col['media_filename']);
			$hrf = '';
			
			if ($ext === 'png' || $ext === 'jpg' || $ext === 'gif' || $ext === 'bmp' || $ext === 'jpeg')
			{
				$out.= '<span class="fa fa-fw fa-picture-o"></span> &nbsp;';
				$out.= '<a href="#" data-toggle="modal" data-target="#myModal" onclick="render(\''.$md5.'\',\'/uploads/'.
				$col['media_filename'].'\');$(\'#render-image\').attr(\'src\',\'/uploads/'.
				$col['media_filename'].'\')">';
				$hrf = '</a>';
			}			
			if ($ext === 'doc' || $ext === 'docx' || $ext === 'pdf' || $ext === 'txt' || $ext === 'rtf')
			{
				$out.= '<span class="fa fa-fw fa-file"></span> &nbsp;';
				$out.= '<a target="_blank" href="/uploads/'.
				$col['media_filename'].'">';
				$hrf = '</a>';
			}		
			if ($ext === 'mp3' || $ext === 'wav' || $ext === 'ogg' || $ext === 'flac' || $ext === 'wma')
			{
				$out.= '<span class="fa fa-fw fa-music"></span> &nbsp;';
				$out.= '<a href="#" onclick="play(\'/uploads/'.$col['media_filename'].'\',\'audio/'.$ext.'\',\''.
				$col['media_filename'].'\')">';
				$hrf = '</a>';
			}
			if ($ext === 'mp4' || $ext === 'avi' || $ext === 'oggv' || $ext === 'mkv')
			{
				$out.= '<span class="fa fa-fw fa-play"></span> &nbsp;';
				$out.= '<a href="#" onclick="play(\'/uploads/'.$col['media_filename'].'\',\'video/'.$ext.'\',\''.
				$col['media_filename'].'\')">';
				$hrf = '</a>';
			}

			$out.= ''.$col['media_title'].$hrf;
			$out.= '</div><div class="col-lg-8 col-md-6 col-sm-6 col-xs-12"><span class="span160">'.$size.'</span><span class="fa fa-fw fa-pencil"></span><span class="fa fa-fw fa-envelope"></span>';
			$out.= $del.'</div></div></li>';
		}
		$out.= '</ul>';
		$out.= '</div>';
		return $out;
	}
	
}

/*
// Usage:
// Load the original image
$image = new SimpleImage('lemon.jpg');

// Resize the image to 600px width and the proportional height
$image->resizeToWidth(600);
$image->save('lemon_resized.jpg');

// Create a squared version of the image
$image->square(200);
$image->save('lemon_squared.jpg');

// Scales the image to 75%
$image->scale(75);
$image->save('lemon_scaled.jpg');

// Resize the image to specific width and height
$image->resize(80,60);
$image->save('lemon_resized2.jpg');

// Resize the canvas and fill the empty space with a color of your choice
$image->maxareafill(600,400, 32, 39, 240);
$image->save('lemon_filled.jpg');

// Output the image to the browser:
$image->output();
*/