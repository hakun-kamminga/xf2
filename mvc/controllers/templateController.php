<?php
/**
 * Dashboard
*/
$template = true;

class TemplateController extends X2Controller
{
	public function __construct()
	{
		parent::Auth();	
	}
	public function GetImageSwatches()
	{
		$db = new \Database();
		$db->Query("SELECT * FROM media WHERE (
			media_filename LIKE '%.jpg' OR  
			media_filename LIKE '%.png' OR  
			media_filename LIKE '%.gif'	) ORDER BY media_title");
		return $db->resultset();
	}

}