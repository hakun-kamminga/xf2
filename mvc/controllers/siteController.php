<?php
class SiteController extends X2Controller
{
	public $attributes = [];
	public function __construct()
	{
		/**
		 * SiteController only works on Front Pages - NOT backend!
		*/
		$this->attributes = self::Page();
	}
	public static function Nav()
	{
		include(WEBROOT."/mvc/modules/nav.php");
	}
	public static function Page()
	{
		$db = new Database();
		if (empty($_GET['q']))
		{
			$db->query("SELECT * FROM page WHERE page_homepage=1 AND page_active = 1 LIMIT 1");
			$pg = $db->single();
		}
		else
		{
			$db = new Database();
			$db->query("SELECT * FROM page WHERE page_slug = :page_slug AND page_active = 1");
			$db->bind(":page_slug",$_GET['q']);
			$pg = $db->single();
		}
		
		if (empty($pg))
		{
			/**
			 * When a page is not found, we need to compare the URL to innate functions.
			 * 
			 * These should be defined somewhere else, but for now we hard code them:
			 * 
			*/
			switch($_GET['r'])	// Route (full), not Query(first URI component after initial / )
			{
				case "/user/profile":
				return [
					'page_title'   => 'Profile',
					'page_content' => '<p>Profile page under construction.</p><pre>'.print_r($_SESSION,true).'</pre>'
				];
				break;
				default:
				return [
					'page_title'   => '404 Not Found',
					'page_content' => '<p>Sorry, that page was not found.</p>'
				];			
				break;
			}
		}
		else
		{
			return $pg;
		}
	}
	public function LoginFB()
	{
		$fb = new Facebook\Facebook([
		  'app_id'     => \Utils::GetConfig('FB App Id')['setting_value'], // Replace {app-id} with your app id
		  'app_secret' => \Utils::GetConfig('FB App Secret')['setting_value'],
		  'default_graph_version' => 'v2.10',
		  ]);
		
		$helper = $fb->getRedirectLoginHelper();
		
		$permissions = ['email']; // Optional permissions
		$loginUrl = $helper->getLoginUrl('https://hakun-kamminga.com/auth.php', $permissions);
		
		echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
		exit();
	}
	public function AuthenticateFB()
	{
		$fb = new Facebook\Facebook([
		  'app_id'     => \Utils::GetConfig('FB App Id')['setting_value'], // Replace {app-id} with your app id
		  'app_secret' => \Utils::GetConfig('FB App Secret')['setting_value'],
		  'default_graph_version' => 'v2.10',
		  ]);

		$helper = $fb->getRedirectLoginHelper();
		
		try {
		  $accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}
		
		if (! isset($accessToken)) {
		  if ($helper->getError()) {
		    header('HTTP/1.0 401 Unauthorized');
		    echo "Error: " . $helper->getError() . "\n";
		    echo "Error Code: " . $helper->getErrorCode() . "\n";
		    echo "Error Reason: " . $helper->getErrorReason() . "\n";
		    echo "Error Description: " . $helper->getErrorDescription() . "\n";
		  } else {
		    header('HTTP/1.0 400 Bad Request');
		    echo 'Bad request';
		  }
		  exit;
		}
		// Logged in
		//echo '<h3>Access Token</h3>';
		//var_dump($accessToken->getValue());
		
		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();
		
		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);
		//echo '<h3>Metadata</h3>';
		//var_dump($tokenMetadata);
		
		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId(\Utils::GetConfig('FB App Id')['setting_value']); // Replace {app-id} with your app id
		// If you know the user ID this access token belongs to, you can validate it here
		//$tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration();
		
		if (! $accessToken->isLongLived()) {
		  // Exchanges a short-lived access token for a long-lived one
		  try {
		    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
		  } catch (Facebook\Exceptions\FacebookSDKException $e) {
		    echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
		    exit;
		  }
		
		  //echo '<h3>Long-lived</h3>';
		  //var_dump($accessToken->getValue());
		}
		
		$_SESSION['priv'] = 'pending';
		$_SESSION['fb_access_token'] = (string) $accessToken;
		
		$fb->setDefaultAccessToken($_SESSION['fb_access_token']);

		try 
		{
		  $response = $fb->get('/me',$_SESSION['fb_access_token'],['fields' => 'id,name,email,address,first_name,last_name']);
		  $userNode = $response->getGraphUser();

		} catch(Facebook\Exceptions\FacebookResponseException $e) 
		{
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}
		return $userNode;
	}
}