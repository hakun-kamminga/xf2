<?php

class Notebook extends X2Controller{
	
	private $note;
	private $id;
	private $notebook;
	
	public function __construct()
	{
		$this->note 	= '';
		$this->id   	= 0;
		$this->notebook = 0;
	}
	public function GetNoteBooks()
	{
		$db = new Database();
		$db->query("SELECT * FROM notebook");
		return $db->resultset();
	}
	public function GetNotes($notebook=null)
	{
		if (empty($notebook))
		{
			return false;
		}
		$db = new Database();
		$db->query("SELECT * FROM note WHERE note_notebook_id = :parent AND note_owner = :owner ORDER BY note_updated DESC");
		$db->bind(':parent',(int)$notebook);
		$db->bind(':owner', (int)$_SESSION['user']['user_id']);
		return $db->resultset();
	}
	public function SaveNote()
	{
		$db  = new Database();
		$db->query("UPDATE note SET note_body = :note,note_updated = :now WHERE note_id = :id");
		$cmd->bind(":note",   $this->note);
		$cmd->bind(":now",    date('Y:m:d H:i:s'));
		$cmd->bind(":note_id",$this->id);
		
		return $cmd->execute();
	}
	public function CreateNote($title = 'Untitled', $note = 'No message')
	{
		$db  = new Database();
		$db->query("INSERT INTO note (note_notebook_id,note_title,note_author,note_body) VALUES (:notebook,:id,:title,:author,:note)");
		$cmd->bind(":notebook", $this->notebook);
		$cmd->bind(":id",		$this->id);
		$cmd->bind(":title",	$title);
		$cmd->bind(":author",	$_SESSION['user']['user_id']);
		$cmd->bind(":note",		$note);
		
		return $cmd->execute();
	}
	public static function EncryptMessage($message,$title = '',$pub)
	{
		$db = new Database();
		openssl_public_encrypt($message,$encrypted,$pub);
		/**
		 * Store encrypted message in DB
		*/
		$sql = "INSERT INTO notes (nb_id,nb_title, nb_description, nb_created,nb_updated,nb_owner) VALUES (0,:title, :desc, :now, :now, :owner)";

		$db->query($sql);
		$cmd->bind(":owner",  $_SESSION['user']['id']);
		$cmd->bind(":title",  $title);
		$cmd->bind(":desc",   base64_encode($encrypted));
		$cmd->bind(":now",    date('Y-m-d H:i:s'));

		$cmd->execute();
		
		unset($message);
		
		die("$encrypted<br>$pub<br>$nu_id");
	}
	public static function DecryptMessage($pg_id,$private_key)
	{
		$db->query("SELECT note_body FROM notes 
		
		WHERE  pg_id = ".intval($pg_id)." 
		AND pg_owner = ".intval($_SESSION['user']['id'])." LIMIT 1");
		$encrypted = $db->single();
		if (!empty($encrypted) && !empty($private_key))
		{
			$pkey = openssl_pkey_get_private($private_key);
			
			openssl_private_decrypt(base64_decode($encrypted['pg_content']), $decrypted, $pkey);
			
			return $decrypted;
		}
		return false;
	}	
}