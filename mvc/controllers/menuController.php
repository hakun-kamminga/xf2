<?php
/**
 * Require Editor to find related pages.
*/
require_once(WEBROOT.'/mvc/controllers/editorController.php');

$menueditor = true;

class MenuController extends X2Controller
{
	public function __construct()
	{
		parent::Auth();	
	}
	public function ListMenus($root = false)
	{
		$db = new Database();
		$db->query("SELECT json_value FROM json WHERE json_key = 'Main Menu'");
		return $db->single();
	}

}