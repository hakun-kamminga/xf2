<?php
/**
 * Require Editor to find related pages.
*/
$formeditor = true;

class FormController extends X2Controller
{
	public function __construct()
	{
		parent::Auth();	
	}
	public function ListForms($root = false)
	{
		$db = new Database();
		$db->query("SELECT * FROM form ORDER BY form_weight ASC");
		return $db->resultset();
	}
	public function GetForm($id)
	{
		$db = new Database();
		$db->query("SELECT * FROM form WHERE form_id = :form_id");
		$db->bind(':form_id',$id);
		return $db->single();
	}
	public function GetFields($id)
	{
		$db = new Database();
		$db->query("SELECT * FROM field WHERE field_form_id = :form_id ORDER BY field_weight ASC");
		$db->bind(':form_id',$id);
		return $db->resultset();
	}
	public function Validate()
	{
		return null;
	}
	public function UpdateFields(array $fields)
	{
		$db = new \Database();
		$bindval = [];
		$bindkey = [];
		$q = "UPDATE `field` SET ";
		foreach($fields as $key=>$val)
		{
			if ($key == 'field_id')continue;	// This is an autonumber.
			if (substr($key,0,5) == 'field')
			{
				$q.= "`$key`  = :".strtolower($key).",\n";
				$bindval[] = $val;
				$bindkey[] = ":$key";
			}
		}
		$q = substr($q,0,-2);
		$q.= " WHERE `field_id` = :field_id LIMIT 1";
		
		$db->query($q);

		for($i=0;$i<count($bindval);$i++)
		{
			$db->bind($bindkey[$i],$bindval[$i]);
		}
		$db->bind(":field_id",$fields['field_id']);
		
		$x = $db->execute();
		$c = $db->rowCount();
	}
	public function InsertFields(array $pfields)
	{
		$db = new \Database();
		$q = [];
		$p = [];
		
		foreach($pfields as $key=>$val)
		{
			if (substr($key,0,5) == 'field')
			{
				$q[] = "`$key`";
				$p[] = ":".strtolower($key);
			}
		}
		$fields = implode(",",$q);
		$values = implode(",",$p);					
		$db->query('INSERT INTO field ('.$fields.') VALUES ('.$values.')');
		
		for($i=0;$i<count($q);$i++)
		{
			$db->bind($p[$i],$pfields[str_replace("`","",$q[$i])]);
		}
		$db->execute();
		$id = $db->lastInsertId();
		$db->query("SELECT * FROM field WHERE field_form_id = ".intval($pfields['field_form_id']));
		
		$temp = $db->resultset();
		$html = $this->RenderFields($temp,$id);
		
		header("Content-Type: text/html");
		echo $html;
		exit();
	}
	public function RenderFields($fields,$id=null)
	{
		$out = '';
		foreach($fields as $field)
		{
			$out.= '
			<form action="/ajax" method="post" onsubmit="return saveFields(this)">
			<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="panel panel-info">
			  <div class="panel-heading" onclick="$(\'#field_config_'.$field['field_id'].'\').slideToggle(\'fast\',function(){toggleArrow(this);})">
			    <h3 class="panel-title"><span id="title-'.$field['field_id'].'">'.$field['field_name'].'</span>&nbsp;<span data-state="closed" id="chevron_'.$field['field_id'].'" class="fa fa-xs fa-chevron-down floatright"></span></h3>
			  </div>
			  <div class="panel-body" data-id="'.$field['field_id'].'" id="field_config_'.$field['field_id'].'" style="display:none">
			  
				  <div class="form-group row">
					 <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 proclaim" style="padding-top:9px">Field Type</div>
					 <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
					  <select name="field_type" class="form-control">
					  <option value="">Select...</option>
					  <option value="text"'.	($field['field_type']=='text'?' selected':'').		'>Text</option>
					  <option value="email"'.	($field['field_type']=='email'?' selected':'').		'>Email</option>
					  <option value="password"'.($field['field_type']=='password'?' selected':'').	'>Password</option>
					  <option value="textarea"'.($field['field_type']=='textarea'?' selected':'').	'>Textarea</option>
					  <option value="number"'.	($field['field_type']=='number'?' selected':'').	'>Number</option>
					  <option value="number"'.	($field['field_type']=='upload'?' selected':'').	'>File Upload</option>
					  <option value="lookup"'.	($field['field_type']=='lookup'?' selected':'').	'>Lookup</option>
					  </select>
					 </div>
				  </div>
			  
				  <div class="form-group row">
					 <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 proclaim" style="padding-top:9px">Field Name</div>
					 <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
					  <input type="text" class="form-control" name="field_name" value="'.htmlentities($field['field_name']).'" onkeyup="$(\'#title-'.$field['field_id'].'\').html(this.value)">
					 </div>
				  </div>
				  <div class="form-group row">
					 <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 proclaim" style="padding-top:9px">Field Default</div>
					 <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
					  <input type="text" class="form-control" name="field_default" value="'.htmlentities($field['field_default']).'">
					 </div>
				  </div>
				  <div class="form-group row">
					 <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 proclaim" style="padding-top:9px">Field Maxlength</div>
					 <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
					  <input type="text" class="form-control" name="field_maxlength" value="'.htmlentities($field['field_maxlength']).'">
					 </div>
				  </div>
				  <div class="form-group row">
					 <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 proclaim" style="padding-top:9px"></div>
					 <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
					  <label for="field_required_'.$field['field_id'].'"><input type="checkbox" id="field_required_'.$field['field_id'].'" value="1" name="field_required"'.($field['field_required']=='1'?' checked':'').'> Required Field</label>
					 </div>
				  </div><hr>
				  <button class="btn btn-success btn-sm"><span class="fa fa-save"></span> Save</button>				  
				  <div style="width:100px;display:none;float:right" id="spinner_'.$field['field_id'].'"><i class="fa fa-spinner fa-spin fa-1x fa-fw"></i> Saving....</div>

			  </div>
			</div>
			</div>
			  
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			
			</div>
			</div><!-- /row -->
			<input type="hidden" name="model" value="field">
			<input type="hidden" name="field_id" value="'.$field['field_id'].'">
			</form>';
			if ($id!==null)$out.= '
			<script>
			setTimeout("$(\'#field_config_'.$id.'\').slideDown(\'fast\',function(){toggleArrow(this);})",200);
			</script>';
		}
		return $out;
	}
}