<?php
class EditorController
{
	public $model;
	public $pages;
	public function __construct()
	{
		$this->model = [];
	}
	public function LoadPage($id)
	{
		$db = new Database();
		$db->query("SELECT * FROM page WHERE page_id = :id");
		$db->bind(":id",$id);
		$row = $db->single();
		return $row;
	}
	public function LoadPageIndex()
	{
		$db = new Database();
		$db->query("SELECT page_id,page_title,page_slug,page_active,page_homepage,page_created,page_updated FROM page ORDER BY page_weight ASC");
		return $db->resultset();
	}
	public function Dashboard()
	{
		$p = $this->LoadPageIndex();
		if (!empty($p))
		{
			$o = '<div class="crow"><ul class="list-group" id="browser">';
			foreach($p as $page)
			{
				$o.= '<li class="list-group-item" data-id="'.$page['page_id'].'">
				<div class="row">
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div style="width:25px;display:inline-block">'.
				($page['page_homepage']==1?'<span class="fa fa-home fa-lg"></span>':'<span class="fa fa-folder-o fa-lg"></span>').'
				</div> <a href="/admin/editor/'.$page['page_id'].'"><strong>'.htmlentities($page['page_title']).'</strong></a>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12"><a href="/'.
				$page['page_slug'].'" target="_blank">'.$page['page_slug'].'</a>
				</div>
				<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12"><label for="active_'.$page['page_id'].'"><input onclick="activate(this)" type="checkbox" id="active_'.$page['page_id'].'" '.(!empty($page['page_active'])?'checked':'').'> Active</label></div> 
				<div class="col-xl-4 col-lg-3 col-md-3 col-sm-12 col-xs-12 rightalign">'.(empty($page['page_updated'])?'Created on <em>'.$page['page_created'].'</em>':'Updated on <em>'.$page['page_updated']).'</em></div>
				</div>
				</li>';
			}
			$o.= '<ul></div>';
		}
		return $o;
	}
}