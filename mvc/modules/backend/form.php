<?php

$formctl = new FormController();

if (!empty($_POST['form_id']))
{
	$fields = $formctl->GetFields($_POST['form_id']);
	$parent = $formctl->GetForm($_POST['form_id']);
	$legend = 'Editing '.$parent['form_title'];
}
else 
{
	$legend = 'Available forms';
	$form    = $formctl->ListForms();
}
?>
</div><!-- end .container from layout -->
<div class="row">
<div class="container">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
 <fieldset id="add_form" style="display:none">
 	<legend id="legend-title"><span id="new-form-title">New Form</span> <span class="fa fa-window-close floatright" onclick="$('#add_form').slideToggle();return false"></span></legend>
 	<form action="/" onsubmit="return addForm(this)" method="post" id="new_form">
 	<div class="form-group">
 		<input type="text" class="form-control" name="form_title" id="form-title" placeholder="Title">
 	</div>	
 	<div class="form-group">
 		<textarea class="form-control" name="form_description" id="form-description" placeholder="Description"></textarea>
 	</div>	
 	<div class="form-group">
 		<button class="btn btn-default" id="create-button">Create</button>
 		<button class="btn btn-danger floatright" onclick="$('#add_form').slideToggle();return false">Delete</button>
 	</div>	
 	<input type="hidden" name="model" value="form">
 	<input type="hidden" name="form_id" id="form-id" value="">
 	</form>
 	
 </fieldset>
 <fieldset><legend><?=$legend ?></legend>
  
<?php
if (empty($_POST['form_id']))
{
	echo '<ul class="list-group" id="forms">';
	foreach($form as $row)
	{
		echo('
	 	<li id="form_'.$row['form_id'].'" data-id="'.$row['form_id'].'" class="list-group-item">
	 	 <button style="display:none" class="btn btn-danger" onclick="if(confirm(\'Are you sure?\'))alert(\'Not implemented yet!\')">
	 	  <span class="fa fa-trash"></span>
	 	 </button> <form action="/admin/form" class="pointer" onclick="this.submit()" method="post"><input type="hidden" name="form_id" value="'.$row['form_id'].'">'.
		 $row['form_title'].'
		 <button class="btn btn-xs btn-default floatright" data-form-id="'.$row['form_id'].'" data-form-title="'.
		 $row['form_title'].'"><span class="fa fa-pencil"></span></button></form>
		</li>');
	}
	echo '</ul>';
}
else
{
	echo '<div id="form-fields">';
	print $formctl->RenderFields($fields);
	echo '
	</div><!-- /form-fields -->
	<div class="form-group row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	  <button onclick="saveAll('.$_POST['form_id'].')" class="btn btn-success btn-sm"><span class="fa fa-save"></span> Save all</button>
	  <button onclick="addField('.$_POST['form_id'].')" class="btn btn-default btn-sm"><span class="fa fa-plus"></span> New field</button>
	  <button onclick="delete('.$_POST['form_id'].')" class="floatright btn btn-danger btn-sm"><span class="fa fa-trash"></span> Delete</button>
	 </div>
	</div>';
}

?>
	
 </fieldset>
</div>
</div>
</div>
<div id="bottom-controls" class="navbar-header">
<div class="container">
<button id="add-form" class="btn btn-default btn-sm">
<span class="fa fa-plus fa-sm"></span>&nbsp; New Form
</button>
</div>
</div><!-- Modal for form element view -->

<div id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formElementLabel" aria-hidden="true">
  <div class="modal-dialog">      
    <div class="modal-content">
    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal">&times;</button>
     <h4 class="modal-title">Add Form Field</h4>
    </div>    	
        <div class="modal-body" id="form-element">
        	
		<ul class="nav nav-tabs">
		  <li class="active"><a data-toggle="tab" href="#text-field">Text Field</a></li>
		  <li><a data-toggle="tab" href="#text-area">Text Area</a></li>sa
		  <li><a data-toggle="tab" href="#">Dropdown List</a></li>
		  <li><a data-toggle="tab" href="#">Checkbox</a></li>
		  <li><a data-toggle="tab" href="#">File Upload</a></li>
		</ul>
		<div class="tab-content clearfix">
			
			<div class="tab-pane active" id="text-field">
			 <div class="fe">
			  <form action="/" onsubmit="return addComponent(this)">
				<div class="form-group">
					<input class="form-control" name="field_name" placeholder="Field Name" type="text">
				</div>
				<div class="form-group">
					<input class="form-control" name="field_default" placeholder="Field Default Value" type="text">
				</div>
				<div class="form-group">
					<input class="form-control" name="field_default" placeholder="Max Length" type="number" value="255">
				</div>
				
			  </form>
			 </div>
	        </div>
	
			<div class="tab-pane" id="text-area">
			 <div class="fe">
			  <form action="/" onsubmit="return addComponent(this)">
				<div class="form-group">
					<input class="form-control" name="field_name" placeholder="Field Name" type="text">
				</div>
				<div class="form-group">
					<input class="form-control" name="field_default" placeholder="Field Default Value" type="text">
				</div>
				<div class="form-group">
					<input class="form-control" name="field_rows" placeholder="Field Rows" type="number">
				</div>
				<div class="form-group">
					<input class="form-control" name="field_default" placeholder="Max Length" type="number" value="16777215">
				</div>
				
			  </form>
	         </div>
	        </div>
        
        </div>
        
        
        <div class="container">
         <button onclick="$('#new-component').slideToggle()" class="btn btn-success btn-xs"><span class="fa fa-sm fa-plus"></span></button>
        </div>
    </div>
  </div>
</div>
<script>
function editElements(what){
	
	$('#formModal').modal('show');
}
function toggleArrow(what){
	var id = $(what).data('id');
	var chevron = $('#chevron_'+id).data('state');
	if (chevron == 'closed'){
		$('#chevron_'+id).attr('class','fa fa-xs fa-chevron-up floatright').data('state','open');
	}
	else {
		$('#chevron_'+id).attr('class','fa fa-xs fa-chevron-down floatright').data('state','closed');
	}
	return true;
}
function addField(form_id) {
	$.post('/ajax',{'model':'field','field_form_id':form_id,'field_name':'New Field'},function(data){
		$('#form-fields').html(data);
	});
	return false;
}
function saveFields(what) {
	$('#field_config_'+what['field_id'].value).animate({opacity:0.5},'slow');
	$('#spinner_'+what['field_id'].value).fadeIn();
	$.post('/ajax',$(what).serialize(),function(data){
		
		$('#spinner_'+what['field_id'].value).delay(500).fadeOut();
		$('#field_config_'+what['field_id'].value).delay(500).animate({opacity:1},'slow');
	});
	return false;
}
</script>
<?php $endbody = '<script src="/web/assets/js/form.js"></script>'; ?>