
<?php

$sys      = new SysconfigController();
$settings = $sys->ListOptions();

?>

<form class="form-horizontal" onsubmit="return saveSettings(this)">
<fieldset>
<?php
$cluster = '';

foreach($settings as $setting)
{
	if ($setting['setting_cluster_id'] != $cluster)
	{
		$cluster = $setting['setting_cluster_id'];
		echo '</fieldset><fieldset style="clear:both"><legend>'.$setting['setting_cluster_id'].'</legend>
		<ul class="list-group">';
	}
	$cfg = '';
	$ico = '<input type="hidden" name="setting_icon" id="setting_icon" value="'.$setting['setting_icon'].'"><span class="blue fa-fw '.$setting['setting_icon'].'"></span> &nbsp;';
	
	if ($setting['setting_has_config'])$cfg = '<span class="floatright"><span class="fa fa-cogs"></span> Configure</button></span>';
	
	if ($setting['setting_bool'] == 1)
	{
		echo '
		<li class="list-group-item">
		<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-4 col-xs-7"><label for="setting_'.$setting['setting_id'].'">'.$ico.' '.$setting['setting_name'].'</label></div>
		<div class="col-lg-6 col-md-6 col-sm-8 col-xs-5">';

		echo '
		    <input type="radio" name="setting_'.$setting['setting_id'].'" id="setting_'.$setting['setting_id'].'" autocomplete="off" value="1" '.(!empty($setting['setting_value'])?'checked':'').'> On
		    <input type="radio" name="setting_'.$setting['setting_id'].'" id="setting_'.$setting['setting_id'].'" autocomplete="off" value="0" '.(empty($setting['setting_value'])?'checked':'').'> Off';
		    
		echo $cfg.'</div></li>';
	}
	else if($setting['setting_optgroup'] == 0)
	{
		echo '<li class="list-group-item">
		      <div class="row">';
		echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><label for="setting_'.$setting['setting_id'].'">'.$ico.' '.$setting['setting_name'].'</label></div>';
		echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><input type="text" class="form-control" id="setting_'.$setting['setting_id'].'" name="setting_'.$setting['setting_id'].'" value="'.htmlentities($setting['setting_value']).'"></div>';
		echo $cfg.'</div></li>';
	}
	else
	{
		echo '<li class="list-group-item">
		      <div class="row">';
		echo '<div class="col-lg-6 col-md-6 col-sm-4 col-xs-7"><label for="setting_'.$setting['setting_id'].'">'.$ico.' <strong>'.$setting['setting_name'].'</strong></label></div>';
		echo '<div class="col-lg-6 col-md-6 col-sm-8 col-xs-5">';
		
		$options = explode("|",$setting['setting_value']);
		
		echo '<select class="form-control" name="setting_'.$setting['setting_id'].'" id="setting_'.$setting['setting_id'].'">';
		
		foreach($options as $option)
		{
			$sel = '';
			if(substr($option,0,1) == '*')
			{
				$sel = 'selected';
				$option = substr($option,1);
			}
			echo '<option value="'.$option.'" '.$sel.'>'.$option.'</option>';
		}

		echo '</select></div>';
		echo $cfg.'</div></li>';
	}
}

?></ul>
</fieldset>

<input type="hidden" id="vendor" name="vendor" value="X2">
<input type="hidden" id="app_id" name="app_id" value="<?=md5('sysconfig') ?>">
<input type="hidden" id="model" name="model" value="settings">

<div id="bottom-controls">
<div class="container">
<button id="save-btn" class="btn btn-success btn-sm">
<span class="fa fa-file fa-sm"></span>&nbsp; Save
</button>
<button id="new-btn" class="btn btn-primary btn-sm"
   data-toggle="modal" 
   data-target="#basicModal"><span class="fa fa-plus fa-sm"></span>&nbsp; New

</button>
</div>
</div>
</form>

<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
   <form action="/" onsubmit="return newSetting(this)">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="closeModal" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add a new Setting</h4>
      </div>
      <div class="modal-body">
        	<div class="form-group">
        		<input type="text" class="form-control" name="setting[setting_name]" placeholder="Setting Name">
        	</div>
        	<div class="form-group">
        		<input type="text" class="form-control" name="setting[setting_value]" placeholder="Setting Value">
        	</div>
        	<div class="form-group">
        		<input type="text" class="form-control" name="setting[setting_cluster_id]" placeholder="Setting Cluster" value="General Settings">
        	</div>
        	<div class="form-group">
        		<label for="_setting_bool">
        		<input type="checkbox" name="setting[setting_bool]" id="_setting_bool" value="1"> Is Boolean
        		</label>
        	</div>
        	<div class="form-group">
        		<label for="_setting_optgroup">
        		<input type="checkbox" name="setting[setting_optgroup]" id="_setting_optgroup" value="1"> Is Select List
        		</label>
        	</div>
        	<div class="form-group">
        		<label for="_setting_root_only">
        		<input type="checkbox" name="setting[setting_root_only]" id="_setting_root_only" value="1"> Root Only
        		</label>
        	</div>
        	<div class="form-group">
        		<label for="_setting_has_config">
        		<input type="checkbox" name="setting[setting_root_only]" id="_setting_has_config" value="1"> Configurable
        		</label>
        	</div>
			<input type="hidden" id="_vendor" name="vendor" value="X2">
			<input type="hidden" id="_app_id" name="app_id" value="<?=md5('sysconfig') ?>">
			<input type="hidden" id="_model" name="model" value="settings">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
   </form>
  </div>
</div>

</div><!-- /container -->