<?php
$menuctl = new MenuController();
$pagectl = new EditorController(); 
$menu = $menuctl->ListMenus();
$page = $pagectl->LoadPageIndex();
$select = '<select class="form-control" onchange="$(\'#mnu_href\').val(this.options[this.selectedIndex].value);this.selectedIndex=0"><option value="">Link to existing page</option>';
foreach($page as $link)$select.= '<option value="'.$link['page_slug'].'">'.$link['page_title'].'</option>';
$select.= '</select>';
?>
<form id="frmEdit" method="post" onsubsmit="return saveForm()">
<div style="clear:both"></div>
<div class="row">
<fieldset><legend>Menu</legend>
<div id="menu">
<div class="row">
<div class="col-6 col-lg-6 col-md-6 col-xs-12">
 <div class="panel panel-primary">
 <div class="panel-heading">Edit / Create Menu Element</div>
 <div class="panel-body">
  <div class="col-lg-12">
	<input type="hidden" name="mnu_icon" id="mnu_icon">
	<div class="form-group">
	<div class="input-group">
	<input type="text" class="form-control" id="mnu_text" name="mnu_text" placeholder="Text">
	    <div class="input-group-btn">
	        <button id="mnu_iconpicker" class="btn btn-default btn-m" data-iconset="fontawesome" data-icon="" type="button"></button>
	    </div>
	</div>
	</div>
	<div class="form-group">
		
	<?=$select ?>
	
	<input type="text" class="form-control" id="mnu_href" name="mnu_href" placeholder="URL">
	</div>
	<div class="form-group">
 	 <input type="text" class="form-control" id="mnu_title" name="mnu_title" placeholder="Text">
	</div>
	</form> 
	</div>
   </div>
  </div>
<div class="panel-footer">
	<div class="row">
		<button type="button" id="btnUpdate" class="btn btn-primary"><i class="fa fa-refresh"></i> Update</button>
		<button type="button" id="btnAdd" class="btn btn-success"><i class="fa fa-plus"></i> Add</button>
	</div>
</div>
</div>

<div class="col-6 col-lg-6 col-md-6 col-xs-12">
<div class="panel panel-primary">
<div class="panel-heading clearfix">Menu</div>
<div class="panel-body" id="cont">

<button id="btnReload" type="button" class="hidden btn btn-default btn-sm">
<i class="fa fa-play fa-sm"></i> Load Data</button>	
	
<ul id="myList" class="sortableLists list-group">

</div>
</div>
<div class="form-group hidden">
<button id="btnOut" type="button" class="btn btn-success"><i class="fa fa-success"></i> Output</button>
</div>    
<div class="form-group hidden">
	<textarea id="out" name="out" class="form-control" cols="50" rows="10"><?=$menu['json_value'] ?></textarea>
</div>
</div>
</div><!-- /row -->	

</div>
</fieldset>
<div id="bottom-controls">
<div class="container">
	<div class="row">
		
		<button id="save-btn" class="btn btn-success btn-sm">
			<span class="fa fa-file fa-sm"></span>&nbsp; Save
		</button>
	</div>
</div>
</div>
<input type="hidden" name="menu_id" id="menu-id" name="menu_id" value="<?=10 ?>">
<input type="hidden" name="mnu_target" id="mnu_target" value="_self">
</form>
<?php $endbody = '	<link rel="stylesheet" type="text/css" href="/web/vendor/bs-iconpicker/css/bootstrap-iconpicker.min.css?'.time().'">
					<script src="/web/assets/js/menu.js?'.time().'"></script>
					<script src="/web/vendor/bs-iconpicker/js/iconset/iconset-fontawesome-4.2.0.min.js"></script>
        			<script src="/web/vendor/bs-iconpicker/js/bootstrap-iconpicker.min.js"></script>'; ?>
