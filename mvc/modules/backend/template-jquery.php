<link rel="stylesheet" type="text/css" href="/vendor/grid-editor-master/dist/grideditor.css">

<style>@import url("//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css")</style>
</div><!-- end container -->
</div><!-- end row -->
<div class="container">
<button class="btn btn-xs btn-default" onclick="$('#admin_navigation,.bs-component').slideToggle()">Toggle Fullscreen</button>
 <div id="grid"></div>
</div>
<div class="container">
<div class="row">
<?php $endbody = '
<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/vendor/grid-editor-master/dist/grideditor-font-awesome.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.js"></script>
<script src="/vendor/grid-editor-master/dist/jquery.grideditor.min.js"></script>

<script>
    $(function() {
        // Initialize grid editor
        $("#grid").gridEditor({
            new_row_layouts: [[12], [6,6], [4,4,4], [9,3], [3,9] ],
            content_types: ["summernote"],
			row_classes: [{label: "White Background", cssClass: "bg-white bgwhite"},{label: "Transparent Background", cssClass: "bg-transparent"}],
		    row_tools: [{
		        title: "Set background image",
		        iconClass: "fa-picture-o",
		        on: { 
		            click: function() {
		                $(this).closest(".row").css("background-image", "url(http://placekitten.com/g/1920/300)");
		            }
		        },
		    }],
		    col_tools: [{
		        title: "Insert a form",
		        iconClass: "fa-check",
		        on: { 
		            click: function() {
		                insert("form",$(this).parent().next());
		            }
		        },
		    },
		    {
		        title: "Clear Contents",
		        iconClass: "fa-eraser",
		        on: { 
		            click: function() {
		                $(this).parent().next().html(\'\');
		            }
		        },
		    }
		    
		    ],			    
        });
        
        // Get resulting html
        var html = $("#grid").gridEditor("getHtml");
        console.log(html);
    });
</script>'; ?>
<script>
var select = null;

function insert(what,target) {
	switch(what) {
		case "form":
		/**
		 * Ough to have a long list of available forms but for now...
		*/
		$(target).html($('#form-1').html());
		break;
	}
}
</script>
    <div class="form-area hidden">  
        <form role="form" id="form-1">
        <br style="clear:both">
                    <h3 style="margin-bottom: 25px; text-align: center;">Contact Form</h3>
    				<div class="form-group">
						<input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="email" name="email" placeholder="Email" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
					</div>
                    <div class="form-group">
                    <textarea class="form-control" type="textarea" id="message" placeholder="Message" maxlength="140" rows="7"></textarea>
                        <span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>                    
                    </div>
            
        <button type="button" id="submit" name="submit" class="btn btn-primary pull-right">Submit Form</button>
        </form>
    </div>
    <style>.bg-white{background-color:white;background-image:none;}
    .bg-transparent{background-color:transparent;background-image:none;}
    </style>