<?php

$t = new TemplateController();
$i = $t-> GetImageSwatches();
?>
</div><!-- /container -->

<div style="min-width:285px;width:20%;float:left;height:600px;overflow:hidden">
<ul class="nav nav-tabs">
  <li class="active h6"><a data-toggle="tab" href="#col-lg">col-lg</a></li>
  <li class="h6"><a data-toggle="tab" href="#col-lg">col-md</a></li>
  <li class="h6"><a data-toggle="tab" href="#col-lg">col-sm</a></li>
  <li class="h6"><a data-toggle="tab" href="#col-lg">col-xs</a></li>
</ul>
<div class="tab-content">
 <div id="col-lg" class="container tab-pane active">
 <fieldset><legend class="proclaim">Insert Columns</legend>
 <button class="u7 btn btn-xs" onclick="addRow([12])">
 	[12]
 </button>
 <button class="u7 btn btn-xs" onclick="addRow([1,10,1])">
 	[1,10,1]
 </button>
 <button class="u7 btn btn-xs" onclick="addRow([6,6])">
 	[6,6]
 </button>
 <button class="u7 btn btn-xs" onclick="addRow([2,8,2])">
 	[2,8,2]
 </button>
 <button class="u7 btn btn-xs" onclick="addRow([4,4,4])">
 	[4,4,4]
 </button><br>
 <button class="u7 btn btn-xs" onclick="addRow([3,3,3,3])">
 	[3,3,3,3]
 </button>
 <button class="u7 btn btn-xs" onclick="addRow([4,8])">
 	[4,8]
 </button>
 <button class="u7 btn btn-xs" onclick="addRow([8,4])">
 	[8,4]
 </button>
 <legend class="proclaim">Row</legend>
 <input type="text" class="grid-control" id="row-id" placeholder="ID">
 <input type="text" class="grid-control" id="row-class" placeholder="Class">
 <br>
 <label for="fluid-row" class="proclaim"><input type="checkbox"  id="fluid-row" onclick="toggleFluid(this)" value="1"> 
  Fluid Container
 </label>
 
 <legend class="proclaim">Column</legend>
 <input type="text" class="grid-control" id="col-id" placeholder="ID">
 <input type="text" class="grid-control" id="col-class" placeholder="Class"><br>
 <br>
 <button class="btn btn-xs btn-primary" onclick="reduceCol()">
 	<span class="fa fa-xs fa-minus-circle"></span>
 </button>
 <button class="btn btn-xs btn-primary" onclick="increaseCol()">
 	<span class="fa fa-xs fa-plus-circle"></span>
 </button>
 <button class="btn btn-xs btn-primary" onclick="addImage()">
 	<span class="fa fa-xs fa-picture-o"></span>
 </button>
 <button class="btn btn-xs btn-primary" onclick="makeEditable()">
 	<span class="fa fa-xs fa-edit"></span>
 </button>
 <button class="btn btn-xs btn-primary" onclick="makeStatic()">
 	<span class="fa fa-xs fa-window-close"></span>
 </button>
 <button class="btn btn-xs btn-primary" onclick="deleteCol()">
 	<span class="fa fa-xs fa-trash"></span>
 </button>
 <legend class="proclaim">Insert Media</legend>
 <div style="max-width:285px">
 <?php
 foreach($i as $img)
 {
 	echo '<img src="/uploads/'.$img['media_filename'].'" onclick="addMedia(this)" class="swatch" alt="'.$media_title.'"> ';
 }
 
 ?>
 </div>
  <div class="form-group">
   <textarea class="form-control" rows="10" cols="20" style="width:100%" id="editor" onkeyup="updateText($(this).val())"></textarea>
  </div>
 </div>
 
 
 
 
 <style>.swatch{height:30px;width:auto;}
 .image-generic{
 	max-width:100%;
 }
 
 </style>
 
 
 
 
 
 
 
 <div id="col-md" class="tab-pane active">
 	
 </div>
 <div id="col-sm" class="tab-pane active">
 	
 </div>
 <div id="col-xs" class="tab-pane active">
 	
 </div>
 </fieldset>   	
	
</div>
</div>
<div id="viewport">
	
</div>


<div class="container">

<?php $endbody = '<script src="/web/assets/js/template.js?'.time().'"></script><link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.js"></script>'; ?>