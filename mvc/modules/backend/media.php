<?php

$MC = new MediaController();

?>

<fieldset>
 <legend style="z-index:10">Media Library
  <button class="btn btn-sm btn-primary floatright" data-toggle="modal" data-target="#uploadModal"><span class="fa-sm fa fa-upload"></span> Upload something</button>
 </legend>
   
  <!-- X2 Tab Content -->
  
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#recent"><span class="fa fa-clock-o"></span> Recent</a></li>
  <li><a data-toggle="tab" href="#documents"><span class="fa fa-file-o"></span> Docs</a></li>
  <li><a data-toggle="tab" href="#images"><span class="fa fa-picture-o"></span> Pics</a></li>
  <li><a data-toggle="tab" href="#music"><span class="fa fa-music"></span> Sound</a></li>
  <li><a data-toggle="tab" href="#videos"><span class="fa fa-video-camera"></span> Vids</a></li>
</ul>

<div class="tab-content">
  <div id="recent" class="tab-pane fade in active">
    <legend>Recent</legend>
    <?=$MC->List($MC->GetRecent()) ?>
  </div>
  <div id="documents" class="tab-pane fade">
    <legend>Documents</legend>
    <?=$MC->List($MC->GetDocuments()) ?>
  </div>
  <div id="images" class="tab-pane fade">
    <legend>Images</legend>
    <?=$MC->List($MC->GetImages()) ?>
  </div>
  <div id="music" class="tab-pane fade">
    <legend>Images</legend>
    <?=$MC->List($MC->GetMusic()) ?>
  </div>
  <div id="videos" class="tab-pane fade">
    <legend>Videos</legend>
    <?=$MC->List($MC->GetVideos()) ?>
  </div>
</div>
   

</fieldset>     
<script>
function hasMeta(filename){
	if (library.length<1)return filename;
	for(var i=0;i<library.length;i++){
		if (library[i].media_filename == filename){
			if (library[i].media_title == '')library[i].media_title = 'Untitled Media';
			return library[i].media_title;
		}
	}
	return filename;
}
function deleteMedia(mid,filename){
	$.post( "/ajax", {model:'media',action:'delete',media:filename},function( data ) {
		console.log('server said' + data);
		$('#'+mid).fadeOut();
	});	
	return true;	
	
}
function getURL(what){
	
	$('#directURL').modal('show');
	$('#view-url').val(what);
	$('#view-url')[0].focus();
	return null;
	
}
function play(what,type,filename){
	
	$('#preview').html('<div style="text-align:center"><span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></span></div>');
	$('#meta-editor').hide();
	var temp = type.split('/');
	var mode = temp[0];
	var desc = 'No Description available. Click Edit Metadata below to add a description';
	$('#media-title').html('Loading meta data for '+filename);
	$('#basicModal').modal('toggle');
	$('#media-filename').val(filename);
	$.post('/ajax',{model:'media',action:'getmeta',media_filename:filename},function(meta){
		console.log('Meta is '+meta);
		if (meta!=false)$('#media-title').html(meta.media_title);
		if (meta!=false)desc = meta.media_description;
		$('#preview').html('<'+mode+' controls><source src="'+what+'" type="'+type+'"></'+mode+'><h3>Summary / Description</h3><p>'+nl2br(desc)+'</p>');
		
	});
}

function saveMeta(what){
	$.post( "/ajax", $(what).serialize(),function( data ) {
		console.log('server said' + data);
		//loadLibrary($('#media-filename').val());
		$('#meta-editor').slideToggle();
		$('#myModal').modal('hide');
	});	
	return false;
}
function render(mid,what)
{
	//mid is the id of the <li> element containing the file.
	var file = what;
	var card = '';
	var temp = what.split('/');
	if (temp[1]!=undefined) {
		file = temp[1];	// Do a JS eq of EndExplode('/')
	}
	card+= '<p class="proclaim" id="meta-title"><i class="fa fa-spinner fa-spin fa-md fa-fw"></i> Loading metadata...</p>';
	card+= '<p id="meta-description"></p>';
	card+= '<span class="fa fa-trash fa-sm"></span><button type="button" class="btn btn-xs btn-default" style="position:fixed;top:10px;right:10px" data-dismiss="modal">&times;</button>';
	$('#preview-card').html(card);

	$.post('/ajax',{model:'media',action:'getmeta',media_filename:what},function(meta){
		console.log('Meta is '+meta);
		if (meta!=false)$('#meta-title').html(meta.media_title);
		if (meta!=false)$('#meta-description').html(meta.media_description);
	});	
	return false;
}
function copyForm(filename){
	$('#media-filename').val(filename);
	$('#preview-card').html($('#meta-editor').html());	
}
</script>
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="closeModal" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Previewing <em id="media-title"></em></h4>
      </div>
      <div class="modal-body" id="preview"></div>
      <div class="modal-body" id="meta-editor" style="display:none">
	   <form action="/" method="post" id="meta-group" onsubmit="return saveMeta(this)">
	   	<div class="form-group">
	   		<input type="text" name="media_title" placeholder="Media Title" class="form-control">
	   	</div>
	   	<div class="form-group">
	   		<textarea class="form-control" name="media_description" placeholder="Media Description / Summary" rows="5" cols="40"></textarea>
	   	</div>
	   	<div class="form-group">
	   		<input type="text" name="media_copyright" placeholder="Media Copyright (e.g. Blind Gurdian)" class="form-control">
	   	</div>
	   	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
	   	 <label for="media-public">	
	   	  <input type="checkbox" data-toggle="checkbox" id="media-public" value="1" name="media_public"> Publically accessible
	   	 </label>
	   	</div>
	   	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
	   	 <label for="media-active">	
	   	  <input type="checkbox" data-toggle="checkbox" id="media-active" value="1" name="media_active"> Mark as active
	   	 </label>
	   	</div>
	   	<button class="btn btn-success btn-sm">Save</button>
	   	<button class="btn btn-danger btn-sm floatright" onclick="if(confirm('Are you sure?'))return deleteMedia()"> <span class="fa fa-trash fa-sm"></span> Delete</button>
	   	<button type="button" class="btn btn-sm btn-danger floatright" data-dismiss="modal" style="position:fixed;top:10px;right:10px">&times;</button>
	   	<input type="hidden" name="model" value="media">
	   	<input type="hidden" name="action" value="addmeta">
	   	<input type="hidden" name="media_filename" id="media-filename">
	   	<input type="hidden" name="media_updated" value="NOW()">
	   	</form>
      </div>
      <div class="modal-footer">
      	<button type="button" class="floatleft btn btn-xs btn-success" onclick="$('#meta-editor').slideToggle()">Edit Metadata</button>
        <button type="button" class="floatright btn btn-xs btn-danger" onclick="if(confirm('Are you sure?'))return deleteMedia()" data-dismiss="modal"><span class="fa fa-sm fa-trash"></span> Delete</button>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="closeModal" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Media uploader</h4>
      </div>
      <div class="modal-body">
		<form id="upload" method="post" action="/ajax" enctype="multipart/form-data">
		<div id="drop">
		    <p class="proclaim">Drop any files here, or use the button below</p>
		    <a class="hidden">Browse</a><label class="btn btn-success" for="upload-file">Choose a file</label>
		    <input class="hidden" type="file" name="upl" id="upload-file" multiple />
		</div>
		
		<ul class="list-group">
		 <!-- The file uploads will be shown here -->
		</ul>
		</form>
      </div>
      <div class="modal-footer hidden">
      	<button type="button" class="btn btn-xs btn-success" onclick="$('#meta-editor').slideToggle()">Edit Metadata</button>
        <button type="button" class="btn btn-xs btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<form action="/admin/media" method="post">
	<input type="submit" class="btn btn-default" value="optimise" name="optimise">
</form>


<?php

if (isset($_POST['optimise']))
{
	$MC->Optimise(WEBROOT.'/uploads');
}

$endbody = '<script src="/assets/js/jquery.knob.js"></script>

<!-- jQuery File Upload Dependencies -->
<script src="/assets/js/jquery.ui.widget.js"></script>
<script src="/assets/js/jquery.iframe-transport.js"></script>
<script src="/assets/js/jquery.fileupload.js"></script>
<!-- Our main JS file -->
<script src="/assets/js/script.js"></script>
<script>

$(document).ready(function(){
	
	//loadLibrary();
	
});
</script>
<!-- Modal for image view -->

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog widescreen">
    <div class="modal-content">
        <div class="modal-body">
            <img src="//placehold.it/1000x600" class="img-responsive" id="render-image">
            <div id="preview-card"></div>
        </div>
    </div>
  </div>
</div>
<!-- Modal for URL view -->
<div id="directURL" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="directURLLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"><p class="modal-title">Direct URL</p></div>
        <div class="modal-body padding-horizontal">
          <p>Click to select, [CTRL+C] to copy.</p>
		  <input type="text" id="view-url" class="form-control" onfocus="this.select()">
        </div>
      </div>
    </div>
  </div>
</div>
';