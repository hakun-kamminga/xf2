<?php

$menuctl = new FormController();

?>  
<div id="system-clock">
	<button onclick="$('#system-clock').fadeToggle()" class="floatright paddingright btn" style="position:absolute;top:0;right:0"><span class="fa fa-close"></span></button>
   <canvas id="clockid" class="CoolClock:swissRail myClock"></canvas>
   <h5 class="proclaim"><?=date('r') ?>
   <br><br><span class="fa fa-calendar-o"></span> 
   <a href="/calendar">4 Events</a>
   <br><br><span class="fa fa-envelope-o"></span> 
   <a href="/calendar">8 New Messages</a></h5>
</div>
<div class="row">
 <div class="container">
 <fieldset><legend>Status</legend>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="min-height:230px">
  	<div class="row">
  		<p class="proclaim">
  		 <span class="span20"><span class="fa fa-circle green"></span></span> System uptime: <span class="timer" data-since="2017-09-01 13:35:11" data-format="human"></span> <span class="floatright paddingright fa fa-lg fa-info-circle blue"></span>	
  		</p>
  		<p class="proclaim">
  		 <span class="span20"><span class="fa fa-circle green"></span></span> System update @ 04:31AM completed <span class="floatright paddingright fa fa-lg fa-info-circle blue"></span>
  		</p>
  		<p class="proclaim">
  		 <span class="span20"><span class="fa fa-circle green"></span></span> Firewall blocks: 104 (normal mode) <span class="floatright paddingright fa fa-lg fa-fire green"></span>
  		</p>
  		<p class="proclaim">
  		 <span class="span20"><span class="fa fa-warning orange"></span></span> Cache fragmentation limit reached <span class="floatright paddingright fa fa-lg fa-cogs blue"></span>	
  		</p>
  		<p class="proclaim">
  		 <span class="span20"><span class="fa fa-warning orange"></span></span> SEO visibility issues need your attention <span class="floatright paddingright fa-lg fa fa-cogs blue"></span>	
  		</p>
  		<p class="proclaim">
  		 <span class="span20"><span class="fa fa-close red"></span></span> Module <em>Twitter Live Feed</em> has crashed.<span class="floatright paddingright fa-lg fa fa-wrench green"></span>	
  		</p>  		
  	</div>
  </div>

  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="min-height:230px">
  	<div class="progress-bars">
	<span class="proclaim">Used disk space</span>
	<div class="progress" style="margin:0"> 
	  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
	</div>
	<span class="proclaim">Avg Page Response Time</span>
	<div class="progress" style="margin:0"> 
	  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width: 5%;"></div>
	</div>
	<span class="proclaim">Memory usage</span>
	<div class="progress" style="margin:0"> 
	  <div class="progress-bar progress-bar-default" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;"></div>
	</div>
	<span class="proclaim">Database usage</span>
	<div class="progress" style="margin:0"> 
	  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;"></div>
	</div>
	<span class="proclaim">Support hours remaining</span>
	<div class="progress" style="margin:0"> 
	  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%;"></div>
	</div>
	</div><!-- /progress-bars -->
  </div>	


 </fieldset>
 <fieldset><legend>Transactions</legend>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  	<div class="row">
  		<p class="proclaim">
  		 <span class="span20"><span class="fa fa-dollar green"></span></span> 11 Transactions completed <span class="floatright paddingright fa fa-credit-card blue"></span>	
  		</p>
  		<p class="proclaim">
  		 <span class="span20"><span class="fa fa-area-chart green"></span></span> Average turnover up 0.2% <span class="floatright paddingright fa fa-info-circle blue"></span>
  		</p>
  		<p class="proclaim">
  		 <span class="span20"><span class="fa fa-envelope green"></span></span> Notifications: 25 <span class="floatright paddingright fa fa fa-info-circle blue"></span>	
  		</p>
  	</div>
  </div>

  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
   	<p class="proclaim">Payments pending: <span class="floatright paddingright green">£2,183</span></p>
   	<p class="proclaim">Payments received today: <span class="floatright paddingright green">£97</span></p>
   	<p class="proclaim">Payments received this month: <span class="floatright paddingright green">£683</span></p>
  </div>
 </fieldset>
 <fieldset>
 	<legend>System Information</legend>
 	
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	<div class="row proclaim">
  	<p><span class="span20"><span class="fa fa-info-circle fa-lg blue"></span></span> X2 Version <span class="floatright paddingright blue">0.1.0.9</span></p>
  	<p><span class="span20"><span class="fa fa-info-circle fa-lg blue"></span></span> License Key  <span class="floatright paddingright blue">l04f-12e-d0b-939</span></p>
  	<p><span class="span20"><span class="fa fa-info-circle fa-lg blue"></span></span> Customer Number <span class="floatright paddingright blue">109 183 001</span></p>  

  	<p class="proclaim"><span class="span20"><span class="fa fa-phone fa-lg"></span></span> Contact Sales <span class="floatright paddingright blue">0117 568 9871</span></p>  
  	<p class="proclaim"><span class="span20"><span class="fa fa-ticket fa-lg"></span></span> Raise a support ticket <span class="floatright paddingright blue">support@x2.org</span></p>  
  	<p class="proclaim"><span class="span20"><span class="fa fa-warning fa-lg"></span></span> Declare an emergency <span class="floatright paddingright blue">0845 800 666</span></p>  	
  </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  	<div id="corporate-identity">
  	<?=\Utils::GetLogo() ?>
  	</div>
  </div>
 </fieldset>
</div><!-- /container -->
</div><!-- /row -->
<div id="bottom-controls" class="navbar-header">
<div class="container">
<button id="add-form" class="btn btn-default btn-sm" onclick="$('#system-clock').fadeToggle()">
<span class="fa fa-clock-o fa-sm"></span>&nbsp; Quick Hide
</button>
</div><!-- /container -->
</div><!-- /row -->
<?php $endbody = '
<script src="/web/assets/js/dashboard.js"></script><!--[if IE]><script type="text/javascript" src="/web/vendor/coolclock/excanvas.js"></script><![endif]-->
<script type="text/javascript" src="/web/vendor/coolclock/coolclock.js"></script>
<script type="text/javascript" src="/web/vendor/coolclock/moreskins.js"></script>'; ?>