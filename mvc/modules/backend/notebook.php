<fieldset>
	<legend>Notebooks</legend>
	<ul class="list-group">
	<?php
	$notebook = new Notebook();
	$books = $notebook->GetNotebooks();
	if (!empty($books))
	{
		foreach($books as $book)
		{
			echo '<li class="list-group-item"><a href="/admin/notebook/'.$book['nb_id'].'">'.$book['nb_title'].'</a></li>';
		}
		echo '<legend>Notes</legend>';
		$id = \Utils::EndExplode("/",$_GET['r']);
		if (!empty($id))
		{
			$notes = $notebook->GetNotes($id);
			if (!empty($notes))
			{
				foreach($notes as $note)
				{
					echo '<li onclick="$(\'#note-'.$note['note_id'].'\').slideToggle()" class="list-group-item"><a href="#note-'.$note['note_id'].'">'.$note['note_title'].'</a></li>';
					echo '<li onclick="$(this).summernote()" id="note-'.$note['note_id'].'" style="display:none" class="list-group-item">'.nl2br($note['note_body']).'</li>';
				}
			}
		}
	}

	$endbody = '
	<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.css" rel="stylesheet">
	<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.js"></script>';
	?>
	</ul>
</fieldset>