
<fieldset>
	<legend>Tools</legend>
	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#world-clocks"><span class="fa fa-clock-o"></span> World Clocks</a></li>
	  <li><a data-toggle="tab" href="#calculator" onclick="$('html, body').animate({ scrollTop: $(document).height() }, 'slow')"><span class="fa fa-calc-o"></span> Calculator</a></li>
	  <li><a data-toggle="tab" href="#validate-emails"><span class="fa fa-envelope"></span> Email Validator</a></li>
	</ul>
</fieldset>
<div class="tab-content">
	<fieldset id="world-clocks" class="tab-pane fade in active">
		<legend>World Time</legend>
		<div id="clock_cet" class="col-lg-2 col-md-3 col-sm-4 col-xs-12"></div>
		<div id="clock_gmt" class="col-lg-2 col-md-3 col-sm-4 col-xs-12"></div>
		<div id="clock_ny" class="col-lg-2 col-md-3 col-sm-4 col-xs-12"></div>
		<div id="clock_st" class="col-lg-2 col-md-3 col-sm-4 col-xs-12"></div>
		<div id="clock_hk" class="col-lg-2 col-md-3 col-sm-4 col-xs-12"></div>
		<div id="clock_mos" class="col-lg-2 col-md-3 col-sm-4 col-xs-12"></div>
	</fieldset>
	<fieldset id="calculator" class="tab-pane fade">
		<legend>Calculator</legend>
		    <div class="col-xl-4 col-md-4 col-sm-6 col-xs-12 calculator" align="center">
	      <div class="row displayBox">
	        <p class="displayText" id="display">0</p>
	      </div>
	      <div class="row numberPad">
	        <div class="col-md-9">
	          <div class="row">
	            <button class="btn clear hvr-back-pulse" id="clear">C</button>
	            <button class="btn btn-calc hvr-radial-out" id="sqrt">√</button>
	            <button class="btn btn-calc hvr-radial-out hvr-radial-out" id="square">x<sup>2</sup></button>
	          </div>
	          <div class="row">
	            <button class="btn btn-calc hvr-radial-out" id="seven">7</button>
	            <button class="btn btn-calc hvr-radial-out" id="eight">8</button>
	            <button class="btn btn-calc hvr-radial-out" id="nine">9</button>
	          </div>
	          <div class="row">
	            <button class="btn btn-calc hvr-radial-out" id="four">4</button>
	            <button class="btn btn-calc hvr-radial-out" id="five">5</button>
	            <button class="btn btn-calc hvr-radial-out" id="six">6</button>
	          </div>
	          <div class="row">
	            <button class="btn btn-calc hvr-radial-out" id="one">1</button>
	            <button class="btn btn-calc hvr-radial-out" id="two">2</button>
	            <button class="btn btn-calc hvr-radial-out" id="three">3</button>
	          </div>
	          <div class="row">
	            <button class="btn btn-calc hvr-radial-out" id="plus_minus">&#177;</button>
	            <button class="btn btn-calc hvr-radial-out" id="zero">0</button>
	            <button class="btn btn-calc hvr-radial-out" id="decimal">.</button>
	          </div>
	        </div>
	        <div class="col-md-3 operationSide">
	          <button id="divide" class="btn btn-operation hvr-fade">÷</button>
	          <button id="multiply" class="btn btn-operation hvr-fade">×</button>
	          <button id="subtract" class="btn btn-operation hvr-fade">−</button>
	          <button id="add" class="btn btn-operation hvr-fade">+</button>
	          <button id="equals" class="btn btn-operation equals hvr-back-pulse">=</button>
	        </div>
	      </div>
	    </div>
	</fieldset>
	<fieldset id="validate-emails" class="tab-pane fade">
		<legend>Verify Email Addresses (Bulk)</legend>
		<form action="/" onsubmit="return tools(this)">
			<input type="hidden" name="model" value="tools">
			<input type="hidden" name="action" value="validate-emails">
			<p class="proclaim">Enter one email address per line to validate.</p>
			<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			 <textarea name="emails" class="form-control" rows="10" cols="40"></textarea>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			 <div id="failed" style="display:none">
			 <pre id="failed-emails" class="code btn-warning"></pre>
			 <button id="fixbtn" class="btn btn-primary btn-sm floatright" onclick="$('#repaired').modal('show');return false">Try to fix</button>
			</div>
			</div>
			</div>
			<div class="form-group">
				<br>
				<button class="btn btn-success btn-sm">Validate</button>
				<label for="trim-emails"> &nbsp;  <input type="checkbox" id="trim-emails" name="trim" value="1"> Trim emails before validating.</label>
				
			</div>
		</form>
	</fieldset>
</div><!-- /tab-content -->

<div class="modal fade" id="repaired" tabindex="-1" role="dialog" aria-labelledby="repaired" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="closeModal" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Fixed emails - best effort</h4>
      </div>
      <div class="modal-body">

		<textarea id="repair" onclick="this.select()" rows="30" cols="80" class="form-control" style="min-height:200px"></textarea>
		
      </div>
      <div class="modal-footer hidden">
      	<button type="button" class="btn btn-xs btn-success" onclick="$('#meta-editor').slideToggle()">Edit Metadata</button>
        <button type="button" class="btn btn-xs btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script>
function tools(what){ 
	$.post( "/ajax", $(what).serialize(),function( data ) {
		console.log(data);
		//loadLibrary($('#media-filename').val());
		if(data['passed'].length>0){
			for(var i=0;i<data['passed'].length;i++){
				console.log('Passed:'+data['passed'][i]);
			}
		}
		if(data['failed'].length>0){
			var emails = "<strong>These emails failed validation:</strong>\n";
			var repair = '';
			for(var i=0;i<data['failed'].length;i++){
				console.warn('Failed:'+data['failed'][i]);
				emails+= '<span style="background:rgba(128,0,0,0.4)">'+data['failed'][i]+"</span>\n";
			}
			$('#failed').fadeIn();
			$('#failed-emails').html(emails);
			
			for(i=0;i<data['repair'].length;i++){
				repair+= data['repair'][i]+'\n';
			}
			$('#repair').val(repair);
			//swal('Sorry, there were failures.','The failed emails are highlighted in red.','error');
		}
	});	
	return false;
}
</script><link rel="stylesheet" href="/web/vendor/jclocksgmt/css/jClocksGMT.css">
<style>
	.btn,a{-webkit-transition:all .5s;-moz-transition:all .5s;transition:all .5s}
	:focus{box-shadow:0 0 0 transparent}.calculator{background:#F0F0F0;box-shadow:2px 3px 6px #212121}
	.displayBox{background:#455A64;padding:0;min-height:220px;overflow:hidden}
	.displayText{margin-top:110px;background:0 0;border:none;color:#FFF;font-size:80px;font-weight:100;padding-right:20px;float:right;display:inline-block;overflow:hidden}.btn-calc,.btn-calc:focus,.btn-operation,.clear,.clear:focus{font-size:25px;font-weight:100;box-shadow:0 0 0 transparent;outline:0}.operationSide{background:#FFF;margin-top:-14px;padding:10px 0 0;border-left:1px solid #E5E5E5}.numberPad{padding-top:14px}.btn-calc,.btn-calc:focus{background:0 0;color:#A2A2A2;width:75px;height:75px;border-radius:100px;border:none}.btn-calc:hover{background:#FFF;color:#455A64}.btn-operation{background:0 0;color:#999;width:100%;height:80.8px;border-radius:0}.btn-operation:focus,.btn-operation:hover{background:#E8E8E8;color:#444}.btn-operation:focus{outline:0}.equals,.equals:focus,.equals:hover{background:#455A64;color:#FFF}.clear,.clear:focus{background:#455A64!important;color:#FFF;width:75px;height:75px;border-radius:100px;border:transparent}.clear:hover{background:#455A64;color:#FFF}.btn-question,.btn-question:focus{z-index:100;position:absolute;top:20px;right:20px;width:40px;height:40px;border-radius:40px;background:0 0;border:2px solid #FFF;padding-top:6px;color:#FFF;font-family:sans-serif;font-size:18px;outline:0}.btn-question:hover{background:rgba(255,255,255,.2);color:#FFF}.hvr-fade:before,.hvr-radial-out:before{background:#FFF!important;transition-duration:.5s}:focus{outline:0!important}::-moz-focus-inner{border:0!important}@media only screen and (max-width:768px){body{padding-top:1.3em}.calculator{height:590px;box-shadow:2px 3px 6px #888}.displayBox{height:150px}.displayText{margin-top:80px;font-size:50px}.btn-calc,.btn-calc:focus{width:70px;height:70px;font-size:18px}.operationSide{padding:0}.btn-operation,.btn-operation:focus{width:60px;height:60px;font-size:18px}.clear,.clear:focus{width:70px;height:70px;font-size:18px}.equals,.equals:focus{width:62px;height:63px;margin-top:1px}.modal-dialog{width:100%}}@media only screen and (min-width:1440px){.container{width:85%}.btn-calc,.btn-calc:focus,.clear,.clear:focus{width:80px;height:80px}.equals,.equals:focus{width:100%;height:96px}}.hvr-radial-out:before{border-radius:100%}</style>
<?php
$endbody = '<script src="/web/vendor/x2/calculate.js"></script>
<script src="/web/vendor/jclocksgmt/js/jquery.rotate.js"></script>
<script src="/web/vendor/jclocksgmt/js/jClocksGMT.js"></script><script>
$(document).ready(function(){
	$("#clock_gmt").jClocksGMT({ title: "London", offset: "0", skin: 5, timeformat: "HH:mm" });
	$("#clock_cet").jClocksGMT({ title: "Berlin", offset: "1", skin: 5, timeformat: "HH:mm" });
	$("#clock_ny").jClocksGMT({ title: "New York", offset: "-4", skin: 5, timeformat: "HH:mm" });
	$("#clock_st").jClocksGMT({ title: "Seattle", offset: "-8", skin: 5, timeformat: "HH:mm" });
	$("#clock_hk").jClocksGMT({ title: "Hong Kong", offset: "8", skin: 5, timeformat: "HH:mm" });
	$("#clock_mos").jClocksGMT({ title: "Moscow", offset: "3", skin: 5, timeformat: "HH:mm" });
});</script>';
?>