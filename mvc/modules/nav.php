<?php

/**
 * Displays navigation.
 * All logic goes here.
*/
$db = new Database();
$db-> query("SELECT json_value FROM json WHERE json_key = 'Main Menu'");
$nav = $db->single(); 
$nav = json_decode($nav['json_value'],true);

if ($_SESSION['priv'] == 'admin')
{
	$nav[] = [
		
		'icon' => 'fa fa-dashboard',
		'text' => 'Dashboard',
		'href' => '/admin',
		];
}
if ($_SESSION['priv'] == 'root')
{
	$nav[] = [
		
		'icon' => 'fa fa-android',
		'text' => '<span class="fa fa-warning orange"></span> Dashboard',
		'href' => '/admin',
		];
}

?>

<nav class="navbar navbar-default">
    <div class="container-fluid">
      
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><?php // \Utils::GetLogo() ?></a>
    </div>      

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">      
      
        <?=\Utils::BuildMenu($nav); ?>

    </div>
    </div>
  </nav>