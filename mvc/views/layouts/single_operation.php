<!DOCTYPE html>
<html>
<head><link href="https://fonts.googleapis.com/css?family=Rajdhani" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/web/bootstrap/bootstrap-yeti.min.css">
<link rel="stylesheet" type="text/css" href="/fa/css/font-awesome.min.css">
	<title><?=$title ?></title>
<style>
@-webkit-keyframes autofill {
    to {
        color: #fff;
        background: transparent;
    }
}
input:-webkit-autofill {
    -webkit-animation-name: autofill;
    -webkit-animation-fill-mode: both;
}

.whiteh1{
	font-weight:100;
	font-size:21px;
	text-transform:uppercase;
	letter-spacing:0px;
	color:white;
	text-align:center;
}
body{
	background-color:transparent;
	margin:0;
	font-family: 'Rajdhani', sans-serif;
	height:100%;
}
html{
	background-size:cover;
	background-color:#000;
	padding:0;
	margin:0;
	height:100%;
	overflow:hidden;
}

p.form-title
{
    font-size: 20px;
    font-weight: 100;
    text-align: center;
    
	border-bottom:1px solid rgba(255,255,255,0.4);
    color: #FFFFFF;
    margin-top: 5%;
    text-transform: uppercase;
    letter-spacing: 4px;
}
.blue{
	color:#07f;
}
form
{
    width: 300px;
    margin: 0 auto;
}

form.login input[type="text"], form.login input[type="password"]
{
	font-family: 'Rajdhani', sans-serif;
    width: 100%;
    margin: 0;
    padding: 5px 10px;
    background: 0;
    border: 0;
    border-bottom: 1px solid #FFFFFF;
    outline: 0;
    font-size: 19px;
    font-weight: normal;
    letter-spacing: 1px;
    margin-bottom: 5px;
    color: #FFFFFF;
    outline: 0;
}

form.login input[type="submit"]
{
	font-family: 'Rajdhani', sans-serif;
    width: 100%;
    font-size: 14px;
    text-transform: uppercase;
    font-weight: 500;
    margin-top: 16px;
    outline: 0;
    cursor: pointer;
    letter-spacing: 1px;
}

form.login input[type="submit"]:hover
{
    transition: background-color 0.5s ease;
}

form.login .remember-forgot
{
    float: left;
    width: 100%;
    margin: 10px 0 0 0;
}
form.login .forgot-pass-content
{
    min-height: 20px;
    margin-top: 10px;
    margin-bottom: 10px;
}
form.login label, form.login a
{
    font-size: 12px;
    font-weight: 400;
    color: #FFFFFF;
}

form.login a
{
    transition: color 0.5s ease;
}

form.login a:hover
{
    color: #2ecc71;
}
#single_operation,#overlay{
	width:100%;
	height:100%;
	position:absolute;
	opacity:0;
	margin:0;
	padding:0;
	background-size:cover;
}
@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
/* Styles */
	form
	{
	    width: 70%;
	    margin: 0 auto;
	}
	form.login input[type="text"], form.login input[type="password"]
	{
		font-size:17px !important;	
	}
}
#remote-ip{
	position:fixed;
	color:white;
	font-size:17px;
	left:10px;
	bottom:10px;
}
#server-time{
	position:fixed;
	color:white;
	font-size:17px;
	right:10px;
	bottom:10px;
    text-align: left;
    width: 136px;	
}
.btn-success{
	background-color:rgba(22, 35, 27, 0.7);;
}
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="/web/assets/jquery-ui/jquery-ui.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<section id="single_operation"></section>
<section id="overlay"></section>
	<?=!empty($header)?'<h1 style="text-align:center">'.$header.'</h1>':'' ?>
	<?=$content ?>
<script src="/web/bootstrap/bootstrap.min.js"></script>
<div id="server-data" style="display:none">
	<div id="remote-ip"><?=$_SERVER['REMOTE_ADDR'] ?><!--<?=$_SERVER['HTTP_USER_AGENT'] ?> --> X2v<?=X2_VERSION ?></div>
	<div id="server-time"><?=date(X2_DATEFORMAT) ?></div>
</div>
</body>
</html>