<style>
body {
	background: #3498db;
}
#PINform input:focus,
#PINform select:focus,
#PINform textarea:focus,
#PINform button:focus {
	outline: none;
}
#PINform {
	background: #ededed;
	position: absolute;
	width: 300px; height: 400px;
	left: 50%;
	margin-left: -180px;
	top: 50%;
	margin-top: -215px;
	padding: 30px;
      -webkit-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
         -moz-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
              box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
}
#PINbox {
	background: #ededed;
	margin: 3.5%;
	width: 92%;
	font-size: 4em;
	text-align: center;
	border: 1px solid #d5d5d5;
}
.PINbutton {
	background: #ededed;
	color: #7e7e7e;
	border: none;
	/*background: linear-gradient(to bottom, #fafafa, #eaeaea);
      -webkit-box-shadow: 0px 2px 2px -0px rgba(0,0,0,0.3);
         -moz-box-shadow: 0px 2px 2px -0px rgba(0,0,0,0.3);
              box-shadow: 0px 2px 2px -0px rgba(0,0,0,0.3);*/
	border-radius: 50%;
	font-size: 1.5em;
	text-align: center;
	width: 60px;
	height: 60px;
	margin: 7px 20px;
	padding: 0;
}
.clear, .enter {
	font-size: 1em;
}
.PINbutton:hover {
 	box-shadow: #506CE8 0 0 1px 1px;
}
.PINbutton:active {
 	background: #506CE8;
	color: #fff;
}
.clear:hover {
 	box-shadow: #ff3c41 0 0 1px 1px;
}
.clear:active {
 	background: #ff3c41;
	color: #fff;
}
.enter:hover {
 	box-shadow: #47cf73 0 0 1px 1px;
}
.enter:active {
 	background: #47cf73;
	color: #fff;
}
.shadow{
      -webkit-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
         -moz-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
              box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
}</style>

<div id="PINcode">
	
	
</div>

<script >$(function() {
	$( "#PINform" ).draggable();
});

$( "#PINcode" ).html(
	"<form action='/admin' method='post' name='PINform' id='PINform' autocomplete='off' tdraggable='true'>" +
		"<input id='PINbox' type='password' value='' name='pin'>" +
		"<br/>" +
		"<input type='button' class='PINbutton' name='1' value='1' id='1'>" +
		"<input type='button' class='PINbutton' name='2' value='2' id='2'>" +
		"<input type='button' class='PINbutton' name='3' value='3' id='3'>" +
		"<br>" +
		"<input type='button' class='PINbutton' name='4' value='4' id='4'>" +
		"<input type='button' class='PINbutton' name='5' value='5' id='5'>" +
		"<input type='button' class='PINbutton' name='6' value='6' id='6'>" +
		"<br>" +
		"<input type='button' class='PINbutton' name='7' value='7' id='7'>" +
		"<input type='button' class='PINbutton' name='8' value='8' id='8'>" +
		"<input type='button' class='PINbutton' name='9' value='9' id='9'>" +
		"<br>" +
		"<input type='button' class='PINbutton clear' name='-' value='clear' id='-' />" +
		"<input type='button' class='PINbutton' name='0' value='0' id='0' />" +
		"<input type='submit' class='PINbutton enter' name='+' value='enter'>" +
	"</form>"
);
$(document).ready(function(){
	
	$('.PINbutton').click(function(){
		if ($(this).val() == 'clear')return clearForm();
		if ($(this).attr('type') == 'submit')return true;
		$('#PINbox').val($('#PINbox').val()+$(this).val());
		return false;
	});
	
});
function clearForm(e){
	//document.getElementById('PINbox').value = "";
	$( "#PINbox" ).val( "" );
}
</script>
<p style="text-align:center">Hint: Liverpool / Belfast / H<sub>2</sub>O</p>