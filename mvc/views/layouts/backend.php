<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bootswatch: Paper</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="/web/assets/style.css?<?=time() ?>" rel="stylesheet">
    <link href="/web/assets/jquery-ui/jquery-ui.min.css" rel="stylesheet">
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	<link href="/fa/css/font-awesome.min.css" rel="stylesheet">    
    <link rel="stylesheet" href="/web/bootstrap/bootstrap<?php 
    
    global $X2;
    
	if (!empty($_SESSION['backend_theme']['css']))
	{
		echo '-'.$_SESSION['backend_theme']['css'];
	}
    
    ?>.min.css" media="screen">
  </head>
<body>
<div class="bs-component">
  <nav class="navbar navbar-<?php if (!empty($_SESSION['backend_theme']['navbar']))echo('inverse'); else echo('default'); ?>">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">XCMS2<span style="font-size:9px;text-transform:uppercase">v<?=X2_VERSION ?></span></a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
        <ul class="nav navbar-nav">
          <li class="active"><a href="/admin/dashboard">Dashboard</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Control Panel <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li class="active"><a href="/admin/sysconfig">Website Configuration</a></li>
              <li><a href="#">Plugins</a></li>
              <li><a href="#">Social Media</a></li>
              <li class="divider"></li>
              <li><a href="#">Connected Apps</a></li>
              <li><a href="#">Statistics / Reports</a></li>
            </ul>
          </li>
          
        </ul>
        <form class="navbar-form navbar-left" role="search" style="display:none">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
        <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hi, <?=$_SESSION['user']['user_name'] ?> <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Profile</a></li>
              <li><a href="#">Notifications <span class="badge floatright">0</span></a></li>
              <li><a href="#">Preferences</a></li>
              <li><a href="/logout">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</div>
<div id="admin_navigation">
 <div class="container-fluid">
   <div class="navbar-header" id="admin-nav">	  
	  <button class="btn btn-default btn-xs" onclick="window.location.href='/'"><span class="navicon fa fa-home fa-sm" title="Dashboard"></span></button>
	  <button class="<?= $X2->route[2]=='dashboard'?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/dashboard'"><span class="navicon fa fa-dashboard fa-sm" title="Dashboard"></span></button>
	  <button class="<?= $X2->route[2]=='sysconfig'?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/sysconfig'"><span class="navicon fa fa-cogs fa-sm" title="System Configuration"></span></button>
	  <button class="<?= $X2->route[2]=='editor'   ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/editor'"><span class="navicon fa fa-pencil fa-sm" title="Page Editor"></span></button>
	  <button class="<?= $X2->route[2]=='menu'     ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/menu'"><span class="navicon fa fa-th-list fa-sm" title="Menu Editor"></span></button>
	  <button class="<?= $X2->route[2]=='media'    ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/media'"><span class="navicon fa fa-upload fa-sm" title="Media Editor"></span></button>
	  <button class="<?= $X2->route[2]=='template' ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/template'"><span class="navicon fa fa-desktop fa-sm" title="Template Editor"></span></button>
	  <button class="<?= $X2->route[2]=='form'     ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/form'"><span class="navicon fa fa-check-square fa-sm" title="Form Editor"></span></button>
	  <button class="<?= $X2->route[2]=='calendar' ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/calendar'" disabled><span class="navicon fa fa-calendar fa-sm" title="Events"></span></button>
	  <button class="<?= $X2->route[2]=='security' ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/security'" disabled><span class="navicon fa fa-lock fa-lg" title="Security"></span></button>
	  <button class="<?= $X2->route[2]=='plugins'  ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/plugins'" disabled><span class="navicon fa fa-plug fa-sm" title="Plugins"></span></button>
	  <button class="<?= $X2->route[2]=='store'    ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/menu'" disabled><span class="navicon fa fa-shopping-bag fa-sm" title="Store"></span></button>
	  <button class="<?= $X2->route[2]=='blog'     ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/blog'" disabled><span class="navicon fa fa-newspaper-o fa-sm" title="Blog"></span></button>
	  <button class="<?= $X2->route[2]=='reports'  ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/reports'" disabled><span class="navicon fa fa-pie-chart fa-sm" title="Reports"></span></button>
	  <button class="<?= $X2->route[2]=='tools'    ?'active ':' ' ?>btn btn-default btn-xs" onclick="window.location.href='/admin/tools'"><span class="navicon fa fa-wrench fa-sm" title="Tools"></span></button>

  </div>
 </div>
</div>
<div class="container">
<?php	
if (isset($this->request))
	include($this->request);
	
?>
<pre style="display:none">
<?php

print_r($_GET);
print_r($_SESSION);

?>
</pre>
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="/web/assets/jquery-ui/jquery-ui.min.js"></script>
<script src="/web/bootstrap/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<?php
if ($_SESSION['priv'] === 'admin' || $_SESSION['priv'] === 'root')
{
	echo '<script src="/web/assets/backend.js"></script>';
}
if (isset($endbody))echo $endbody; ?>
</div><!-- /.container -->
</body>
</html>