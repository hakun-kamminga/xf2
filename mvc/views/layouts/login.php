<!--<link rel="stylesheet" type="text/css" href="/web/bootstrap/bootstrap-spacelab.min.css">-->
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="wrap">
                <p class="form-title" id="form-title">
                    Sign In</p>
                <form class="login" action="/" method="post" onsubmit="return authenticate(this)">
                <input type="text" placeholder="Username" name="user_name" id="user_name">
                <input type="password" placeholder="Password" name="user_password" id="user_password">
                <input type="submit" value="Sign In" name="xa-submit" class="btn btn-success btn-sm">
                <div class="remember-forgot">
                    <div class="row">
                        <div class="col-md-6 forgot-pass-content">
                            <a href="#" class="forgot-pass">Forgot Password?</a>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="model" value="auth">
                </form>
            </div>
        </div>
    </div> 
    <div class="row" style="clear:both">
    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center">
	    		
	        <i class="fa fa-circle-o-notch fa-spin fa-4x fa-fw blue" id="loading" style="display:none"></i>
    		
    	</div>
    </div>
</div>
<script> 
var im = [];
im[0] = '/assets/img/themes/wallpapers/dark/planet.png';
im[1] = '/assets/img/themes/wallpapers/dark/clouds.jpeg';
im[2] = '/assets/img/themes/wallpapers/dark/map.jpg';
im[3] = '/assets/img/themes/wallpapers/dark/night-water.jpg';
im[4] = '/assets/img/themes/wallpapers/mountain.jpg';
im[4] = '/assets/img/themes/wallpapers/waterfall.jpg';
im[5] = '/assets/img/themes/wallpapers/morning-forest.jpg';
im[6] = '/assets/img/themes/wallpapers/sunset-at-sea.jpg';

$(document).ready(function(){
	$('#server-data').fadeIn();
	var r = Math.floor(Math.random()*im.length);
	var i = new Image();
	i.src = im[r];
	$(i).load(function(){
		$('#single_operation').css('background-image','url('+im[r]+')');
		setTimeout('fadeBG()',10);		
	});
	document.getElementById('user_name').select();
});
function fadeBG(){
	$('#single_operation').animate({opacity:1},4000);
}
function authenticate(what) {
	$.post('/ajax',$(what).serialize(),function(data){
		console.log(data);
		if (data.status == 'failed') {
			$('#form-title').html(data.message);
			$('#overlay').css({opacity:1,'background-color':'rgba(64,0,0,0.5)','text-shadow':'none'});
		}
		if (data.status == 'success'){

			$('#form-title').html(data.message);
			$('#overlay').css({opacity:1,'background-color':'rgba(0,32,0,0.8)','text-shadow':'0px 0px 2px rgba(255,255,255,0.7)'});
			$('#loading').fadeIn();
			setTimeout('window.location.href="/admin"',1500);
		}
		
	})
	return false;
}
function time() {
	$.post('/ajax',{service:'get-date'},function(data){
		$('#server-time').html(data);
		setTimeout('time()',1000);
	});
}
time();
</script>