<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bootswatch: Paper</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="/web/bootstrap/bootstrap-cyborg.min.css" media="screen">
  </head>
<body>
<div class="bs-component">
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">XCMS2<span style="font-size:9px;text-transform:uppercase">(pre-alpha 0.01)</span></a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Show stack trace</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Documentation <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a target="_blank" href="https://php.net">PHP.net</a></li>
            </ul>
          </li>
          
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="window.history.go(-1)">Back</a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>
<div class="container">
<div class="alert alert-danger" role="alert">
  <a href="#" class="alert-link"><?= $what['message'] ?></a>
</div>
 <pre><?=$what['debug'] ?>
 
Request:
 <?=print_r($_GET,true) ?>
 </pre>
</body>

<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="/web/bootstrap/bootstrap.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>    

<?php if (isset($endbody))echo $endbody; ?>

</html>