<style>
body {
	background: #3498db;
}
#PINform input:focus,
#PINform select:focus,
#PINform textarea:focus,
#PINform button:focus {
	outline: none;
}
#PINform {
	background: #ededed;
	position: absolute;
	width: 300px; height: 400px;
	left: 50%;
	margin-left: -180px;
	top: 50%;
	margin-top: -215px;
	padding: 30px;
      -webkit-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
         -moz-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
              box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
}
#PINbox {
	background: #ededed;
	margin: 3.5%;
	width: 92%;
	font-size: 4em;
	text-align: center;
	border: 1px solid #d5d5d5;
}
.PINbutton {
	background: #ededed;
	color: #7e7e7e;
	border: none;
	/*background: linear-gradient(to bottom, #fafafa, #eaeaea);
      -webkit-box-shadow: 0px 2px 2px -0px rgba(0,0,0,0.3);
         -moz-box-shadow: 0px 2px 2px -0px rgba(0,0,0,0.3);
              box-shadow: 0px 2px 2px -0px rgba(0,0,0,0.3);*/
	border-radius: 50%;
	font-size: 1.5em;
	text-align: center;
	width: 60px;
	height: 60px;
	margin: 7px 20px;
	padding: 0;
}
.clear, .enter {
	font-size: 1em;
}
.PINbutton:hover {
 	box-shadow: #506CE8 0 0 1px 1px;
}
.PINbutton:active {
 	background: #506CE8;
	color: #fff;
}
.clear:hover {
 	box-shadow: #ff3c41 0 0 1px 1px;
}
.clear:active {
 	background: #ff3c41;
	color: #fff;
}
.enter:hover {
 	box-shadow: #47cf73 0 0 1px 1px;
}
.enter:active {
 	background: #47cf73;
	color: #fff;
}
.shadow{
      -webkit-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
         -moz-box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
              box-shadow: 0px 5px 5px -0px rgba(0,0,0,0.3);
}</style>
<link rel="stylesheet" type="text/css" href="/fa/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/web/bootstrap/bootstrap-slate.min.css">
<script src="/web/bootstrap/bootstrap.min.js"></script>
    <div id="login-overlay" class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel">Login</h4>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-xs-6">
                      <div class="well">
                          <form id="loginForm" method="POST" action="/authenticate/" novalidate="novalidate">
                              <div class="form-group">
                                  <label for="username" class="control-label">Username</label>
                                  <input type="text" class="form-control" id="username" name="username" value="" required="" title="Please enter you username" placeholder="example@gmail.com">
                                  <span class="help-block"></span>
                              </div>
                              <div class="form-group">
                                  <label for="password" class="control-label">Password</label>
                                  <input type="password" class="form-control" id="password" name="password" value="" required="" title="Please enter your password">
                                  <span class="help-block"></span>
                              </div>
                              <div id="loginErrorMsg" class="alert alert-error hide">Wrong username og password</div>
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="remember" id="remember"> Remember login
                                  </label>
                                  <p class="help-block">(if this is a private computer)</p>
                              </div>
                              <button type="submit" class="btn btn-success btn-block">Login</button>
                              <a href="/forgot/" class="btn btn-default btn-block">Help to login</a>
                          </form>
                      </div>
                  </div>
                  <div class="col-xs-6">
                      <p class="lead">Create an account</p>
                      <ul class="list-unstyled" style="line-height: 2">
                          <li><span class="fa fa-check text-success"></span> Receive updates</li>
                          <li><span class="fa fa-check text-success"></span> End poverty</li>
                          <li><span class="fa fa-check text-success"></span> Reject pancakes</li>
                          <li><span class="fa fa-check text-success"></span> Approve pythons</li>
                          <li><a href="/read-more/"><u>Read more</u></a></li>
                      </ul>
                      <p><a href="/register/" class="btn btn-info btn-block">Register</a></p>
                  </div>
              </div>
          </div>
      </div>
  </div>