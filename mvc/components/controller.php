<?php
class X2Controller
{
	private $authenticated = false;
	
	public function __construct()
	{
		self::Auth();
	}
	protected static function Auth()
	{
		if (empty($_SESSION['priv']))
		{
			die("Unauthorized. Reason: No role assigned!");
			return false;
		}
		return true;
	}
}