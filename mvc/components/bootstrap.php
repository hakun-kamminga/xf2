<?php
class Bootstrap
{
	private $col;
	private $row;
	private $container;
	public function __construct()
	{
		$this->col = [];
		$this->row = [];
		
		$this->matrix = [
		
			0 =>	// Row
			[
				'class'  => ['container-fluid','R1'],
				'cols'   => [
				
					'lg' => '12',	
					'md' => '12',	
					'sm' => '12',	
					'xs' => '12',	
				],	
			],
			1 =>	// Row
			[
				'class'  => ['container','R2'],
				'cols'   => [
				
					'lg' => '1,10,1',	
					'md' => '2,8,2',	
					'sm' => '12',	
					'xs' => '12',						
						
				]
			],	
			2 =>	// Row
			[
				'class'  => ['container','R2'],
				'cols'   => [
				
					'lg' => '1,2,2,2,2,2,1',	
					'md' => '2,8,2',	
					'sm' => '12',	
					'xs' => '12',						
						
				]
			],			
			
		];
	}
	public function AddRow($cols = [12])
	{
		
	}
	public function Parse()
	{
		$nl  = "\n";
		$out = $nl.'<div class="content">';
		foreach($this->matrix as $row=>$att)
		{
			$c   = 0;
			$out.= $nl.' <div class="row '.implode(' ',$att['class']).'">'.$nl;

			foreach($att['cols'] as $size=>$cols)
			{
				$c++;
				$out.= '  <div id="R'.$row.'C'.$c.'" class="';
				$div = explode(",",$cols);
				if (is_array($div))
				{
					foreach($div as $col)
					{
						$out.= ' col-'.$size.'-'.$col;
					}
					$out.= '"><!-- COL --></div>'.$nl;
				}
			}
			
			$out.= ' </div>'.$nl;
		}
		$out.= '</div>';
		echo $out;
	}
}