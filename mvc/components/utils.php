<?php
class Utils
{
	public  $input;
	public  $output;
	private $start;
	private $end;
	
	public function __contruct()
	{
		$this->input = null;
		$this->start = microtime();
	}
	public static function MachineName($what)
	{
		$machine_name = preg_replace('@[^a-z0-9-]+@','_', strtolower($what));
		return strtolower($machine_name);
	}
	public static function Humanize($what)
	{
		$what = str_replace("-"," ",$what);
		$what = str_replace("_"," ",$what);
		$syls = explode(" ",$what);
		if (sizeof($syls)>0)
		{
			foreach($syls as $syl)
			{
				$syl = ucfirst($syl);
			}
			$syls = implode(" ",$syl);
		}
		$what = substr($what, 0 , (strrpos($what, ".")));
		return $what;
	}
	// Do a VerifyEmails to see if mailsboxes are available as a companion function.
	public static function ValidateEmails(array $emails)
	{
		$passed = [];
		$failed = [];
		$repair = [];
		foreach($emails as $email)
		{
			if(!empty($_POST['trim']))$email = trim($email);
			$email = str_replace("\n","",$email);				// The way it's submitted is in a textarea split by newlines.
			$email = str_replace("\r","",$email);				// The way it's submitted is in a textarea split by return carriages on window$.
			if(empty($email))continue;
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) 
			{
    			$passed[] = $email;
				$repair[] = $email;
			}
			else
			{
				$failed[] = $email;
				$trytofix = self::Trim($email);
				if (!filter_var($trytofix, FILTER_VALIDATE_EMAIL))
				{
					$trytofix = base64_encode($email)."@invalid.org";
				}
				$repair[] = $trytofix;
			}
		}
		header("Content-Type:application/json");
		die(json_encode(
			[
				'passed'=>$passed,
				'failed'=>$failed,
				'repair'=>$repair,
			]
		));
	}
	public static function Slug($what)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $what);
		
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		
		// trim
		$text = trim($text, '-');
		
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		
		// lowercase
		$text = strtolower($text);
		
		if (empty($text)) {
			return 'n-a';
		}
		return $text;
	}
	public static function Enqueue($what)
	{
		static $queue = [];
		$queue[] = $what;
		echo '<pre>'.print_r($queue).'</pre>';
	}
	public static function Exception($what)
	{
		include(WEBROOT."/mvc/views/layouts/exception.php");
		exit();
	}
	public static function SafeHtml($what)
	{
		$what = str_replace("<?","<code>",$what);
		$what = str_replace("<?php","<code>",$what);
		$what = str_replace("?>","</code>",$what);
		return $what;
	}
	public static function ParseShortcodes($what=null)	// Must be public as it is called from Core context in most cases.
	{
		if (empty($what))
		{
			return [];
		}
		require('parser.php');
		return(Parser::parse_shortcodes($what));
	}
	public static function DateFormat($what)
	{
		$unix = strtotime($what);
		return date(X2_DATEFORMAT,$unix);
	}
	public static function SqlDate($what)
	{
		return date('Y-m-d H:i:s',strtotime($what));
	}
	public static function MustBeInFuture($when,$offset = 24*3600)
	{
		if (is_integer($when))$unix = $when;
		else $unix = strtotime($when);
		if ($unix<time())
		{
			return date(X2_DATEFORMAT,(time()+$offset));
		}
		return $when;
	}
	public static function GetConfig($what)
	{
		$db = new Database();
		$db->query("SELECT * FROM settings WHERE setting_name = :name LIMIT 1");
		$db->bind(':name',$what);
		return $db->single();
	}
	public static function EndExplode($delim=",",$what=null)
	{
		if (empty($what))return null;
		$tmp = explode($delim,$what);
		return end($tmp);
	}
	public static function Trim($what)
	{
		$what = trim($what);
		$what = str_replace("\n","",$what);
		$what = str_replace("\t","",$what);
		return str_replace("\r","",$what);
	}
	public static function Ago($datetime, $full = false) {
		$now = new \DateTime;
		$ago = new \DateTime($datetime);
		$diff = $now->diff($ago);
	
		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;
	
		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}
	
		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}	
	public static function BuildMenu(array $menu_array, $is_sub=FALSE,$level = 0)
	{
		$ul_attrs = $is_sub ? 'class="dropdown-menu"' : 'class="nav navbar-nav"';
		$menu = "\n<ul $ul_attrs>";
		
		foreach($menu_array as $id => $attrs) 
		{
			$sub = isset($attrs['children']) 
			 ? self::BuildMenu($attrs['children'], TRUE,$level+1) 
			 : null;
			
			$extra = '';
			
			if (empty($attrs['href'])) $attrs['href'] = '#';
			if (empty($attrs['text'])) $attrs['text'] = 'Untitled Link';
			if ($level>0)$extra = ' dropdown-submenu';
			 

			$li_attrs = $sub ? 'class="dropdown '.$extra.'"' : '';
			$a_attrs  = $sub ? 'class="dropdown-toggle" data-toggle="dropdown"' : null;
			$caret    = $sub ? '<span class="fa fa-caret-right pullright"></span>' : '';
			$icon     = empty($attrs['icon'])?:'<span class="'.$attrs['icon'].'"></span> &nbsp;';
			$menu .= "\n <li $li_attrs >";
			$menu .= "\n  <a href='${attrs['href']}' $a_attrs>$icon ${attrs['text']}</a> $sub";
			$menu .= "\n </li>";
		}
		
		return $menu . "</ul>";
	}
	public static function GetLogo()
	{
		$logo = self::GetConfig('Site Logo');
		if (!empty($logo))
		{
			return '<img class="logo" alt="'.self::GetConfig('Site Strapline')['setting_value'].'" src="'.$logo['setting_value'].'">';
		}
		return '';
	}
	public static function EventLog($msg,$env)
	{
		return true;
	}
	public static function ToMB($size)
	{
		$unit=array('B','Kb','MB','GB','TB','PETABYTES');
		return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
	}
}