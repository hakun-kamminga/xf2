<?php
class AjaxHandler
{
	public $request;
	public function __construct()
	{
		//print_r($_POST);
	}
	public function ScanRequirements()
	{
		/**
		 * See if this is Vendor related.
		 * If it is, invoke Ajax for relevant vendor / plugin.
		 * */
	}
	public function Parse()
	{
		/**
		 * File upload. 
		 * Penetration testing will be required at some stage.
		*/
		if (!empty($_FILES))
		{
			$allowed = \Utils::GetConfig('Allowed Filetypes');
			if (is_array($allowed))
			{
				$allowed = explode(",",$allowed['setting_value']);

				if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){
				
				    $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
				
				    if(!in_array(strtolower($extension), $allowed))
				    {
				    	header('Content-Type: application/json');
				        echo json_encode([
				       
					    	'status' => 'error',
					    	'info'   => 'Failed to upload '.$_FILES['upl']['name'],
					    	'reason' => 'Invalid / forbidden file type',
				       
				        ]);
				        exit;
				    }
				
				    if(move_uploaded_file($_FILES['upl']['tmp_name'], WEBROOT.'/uploads/'.$_FILES['upl']['name']))
				    {

				    	$mt = mime_content_type(WEBROOT.'/uploads/'.$_FILES['upl']['name']);
				    	$db = new Database();
				    	$db->query("INSERT INTO media (
				    	media_filename,
				    	media_title,
				    	media_mime,
				    	media_filesize,
				    	media_uploaded,
				    	media_uploaded_by) VALUES 
				    	(:filename,:title,:mime,:filesize,:date,:uploaded_by)");

				    	$db->bind(':filename',	$_FILES['upl']['name']);
				    	$db->bind(':title',		\Utils::Humanize($_FILES['upl']['name']));
				    	$db->bind(':mime',		$mt);
				    	$db->bind(':filesize',	(int)$_FILES['upl']['size']);
				    	$db->bind(':date',		date('Y-m-d H:i:s'));
				    	$db->bind(':uploaded_by',100);

				    	$db->execute();
				    	
				    	header('Content-Type: application/json');
				        echo json_encode([
				       
					    	'status' => 'success',
					    	'info'   => 'Successfully uploaded '.$_FILES['upl']['name'],
					    	'reason' => 'OK',
					    	'path'   => '/uploads/'.$_FILES['upl']['name'],
				       
				        ]);
				        exit;
					}
				}
			}
			exit();	
		}
		/**
		 * Generic Ajax, not requiring authentication.
		*/
		if (!empty($_POST['service']))
		{
			switch($_POST['service'])
			{
				case "get-date":
				die(date(X2_DATEFORMAT,time()));
				break;
			}
		}
		switch($_POST['model'])
		{
			case "tools":
			switch($_POST['action'])
			{
				case "validate-emails":
				if (!empty($_POST['emails']))
				{
					$_POST['emails'].= "\n";
					
					$lines = explode("\n",$_POST['emails']);
					\Utils::ValidateEmails($lines);
				}
				exit();
				break;
			}
			case "form":
			$db = new Database();
			if (!empty($_POST['action']))
			{
				switch($_POST['action'])
				{
					case "load":
					$db->query("SELECT * FROM form WHERE form_id = :id");
					$db->bind(':id',$_POST['form_id']);
					$form = $db->single();
					header("Content-Type:application/json");
					die(json_encode($form));
					break;
				}
			}
			if (empty($_POST['form_id']))
			{
				$q = [];
				$p = [];
				
				foreach($_POST as $key=>$val)
				{
					if (substr($key,0,4) == 'form')
					{
						$q[] = "`$key`";
						$p[] = ":".strtolower($key);
					}
				}
				$fields = implode(",",$q);
				$values = implode(",",$p);					
				$db->query('INSERT INTO form ('.$fields.') VALUES ('.$values.')');
				
				for($i=0;$i<count($q);$i++)
				{
					$db->bind($p[$i],$_POST[str_replace("`","",$q[$i])]);
				}
				$db->execute();
				$id = $db->lastInsertId();
			}
			else
			{
				$bindval = [];
				$bindkey = [];
				$q = "UPDATE form SET ";
				foreach($_POST as $key=>$val)
				{
					if ($key == 'form_id')continue;	// This is an autonumber.
					if (substr($key,0,4) == 'form')
					{
						$q.= "`$key`  = :".strtolower($key).",\n";
						$bindval[] = $val;
						$bindkey[] = ":$key";
					}
				}
				$q = substr($q,0,-2);
				$q.= " WHERE `form_id` = :form_id LIMIT 1";
				
				$db->query($q);

				for($i=0;$i<count($bindval);$i++)
				{
					$db->bind($bindkey[$i],$bindval[$i]);
				}
				$db->bind(":form_id",$_POST['form_id']);
				
				$x = $db->execute();
				$c = $db->rowCount();
			}				
			break;
			case "field":
			require WEBROOT."/mvc/controllers/formController.php";
			$form = new FormController();
			
			if (!empty($_POST['field_id']))
			{
				$form->UpdateFields($_POST);
			}
			else
			{
				$form->InsertFields($_POST);
			}
			break;
			case "menu":
			$db = new Database();
			if (empty($_POST['menu_update']))
			{
				if (!empty($_POST['action']))
				{
					switch($_POST['action'])
					{
						case "save-json":
						case "savejson":
						$db->query("UPDATE json SET json_value = :json WHERE json_key = 'Main Menu'");
						$db->bind(':json',$_POST['fields']);
						$db->execute();
						break;
						
						case "loadjson":
						$db->query("SELECT json_value FROM json WHERE json_key = 'Main Menu'");
						$rows = $db->single();
						header("Content-Type:application/json");
						die($out);
						break;
						case "remove":
						case "delete":
						$db->query("DELETE FROM menu WHERE menu_id = :id LIMIT 1");
						$db->bind(":id",$_POST['menu_id']);
						$rows = $db->execute();
						break;
						case "insert":
						$db->query("INSERT INTO menu (menu_title,menu_page_id,menu_raw_url,menu_weight) VALUES (:menu_title,:menu_page_id,:menu_raw_url,10000)");
						$db->bind(":menu_title",$_POST['menu_page_title']);
						$db->bind(":menu_page_id",$_POST['menu_page_id']);
						$db->bind(":menu_raw_url",$_POST['menu_page_slug']);
						$db->execute();
						break;
					}
				}
				else
				{
					/**
					 * No specific action specified. Update / insert as applicable.
					*/
					
					if (empty($_POST['menu_id']))
					{
						$q = [];
						$p = [];
						
						foreach($_POST as $key=>$val)
						{
							if (substr($key,0,4) == 'menu')
							{
								if ($key == 'menu_id')continue;	// This is an autonumber.
								$q[] = "`$key`";
								$p[] = ":".strtolower($key);
							}
						}
						$fields = implode(",",$q);
						$values = implode(",",$p);					
						$db->query('INSERT INTO menu ('.$fields.') VALUES ('.$values.')');
						
						for($i=0;$i<count($q);$i++)
						{
							$db->bind($p[$i],$_POST[str_replace("`","",$q[$i])]);
						}
						$db->execute();
						$id = $db->lastInsertId();
					}
					else
					{
						$bindval = [];
						$bindkey = [];
						$q = "UPDATE menu SET ";
						foreach($_POST as $key=>$val)
						{
							if ($key == 'menu_id')continue;	// This is an autonumber.
							if (substr($key,0,4) == 'menu')
							{
								$q.= "`$key`  = :".strtolower($key).",\n";
								$bindval[] = $val;
								$bindkey[] = ":$key";
							}
						}
						$q = substr($q,0,-2);
						$q.= " WHERE `menu_id` = :menu_id LIMIT 1";
						
						$db->query($q);
						print_r($bindval);
						echo $q;
						for($i=0;$i<count($bindval);$i++)
						{
							$db->bind($bindkey[$i],$bindval[$i]);
						}
						$db->bind(":menu_id",$_POST['menu_id']);
						
						$x = $db->execute();
						$c = $db->rowCount();
					}						
					
				}
			}
				
			break;
			case "settings":
			$db = new Database();
			/**
			 * HK: We should make this consistent.
			*/
			if (isset($_POST['setting']) && is_array($_POST['setting']))
			{
				if (empty($_POST['setting']['setting_id']))
				{
					/**
					 * INSERT
					*/
					$q = [];
					$r = [];
					foreach($_POST['setting'] as $key=>$val)
					{
						$q[] = "`$key`";
						$p[] = ":".strtolower($key);						
					}
				}
				$db->query("INSERT INTO settings (".implode(",",$q).") VALUES (".implode(",",$p).")");
				for($i=0;$i<count($q);$i++)
				{
					$db->bind($p[$i],$_POST['setting'][str_replace("`","",$q[$i])]);
				}		
				$db->execute();		
				exit();
			}
			foreach($_POST as $key=>$val)
			{
				if (substr($key,0,8) == 'setting_')
				{
					$tmp = explode("_",$key);
					$sid = isset($tmp[1])?(int)$tmp[1]:-1;
					
					if ($sid>0)
					{
						$db->query("SELECT * FROM settings WHERE setting_id = :id");
						$db->bind(":id",$sid);
						$row = $db->single();
						if (!empty($row['setting_optgroup']))
						{
							$row['setting_value'] = str_replace("*","",$row['setting_value']);
							$arr = explode("|",$row['setting_value']);
							if (sizeof($arr)>0)
							{
								for($i=0;$i<count($arr);$i++)
								{
									if ($arr[$i] == $val)
									{
										/**
										 * If it's a theme setting, activate immediately
										*/
										if ($row['setting_name'] == 'Backend Theme')
										{
											$_SESSION['backend_theme']['css'] = $val;
										}
										$arr[$i] = "*".$val;
									}
								}
								$val = implode("|",$arr);
							}
						}
						
						$db->query("UPDATE settings SET setting_value = :value WHERE setting_id = :id");
						$db->bind(":value",$val);
						$db->bind(":id",$sid);
						$db->execute();
					}
				}
			}
			break;
			case "page":
			$db = new Database();
			if (empty($_POST['page_id']))
			{
				$q = [];
				$p = [];
				
				foreach($_POST as $key=>$val)
				{
					if (substr($key,0,4) == 'page')
					{
						if ($key == 'page_id')continue;	// This is an autonumber.
						$q[] = "`$key`";
						$p[] = ":".strtolower($key);
					}
				}
				$fields = implode(",",$q);
				$values = implode(",",$p);					
				$db->query('INSERT INTO page ('.$fields.') VALUES ('.$values.')');
				
				for($i=0;$i<count($q);$i++)
				{
					$db->bind($p[$i],$_POST[str_replace("`","",$q[$i])]);
				}
				$db->execute();
				$id = $db->lastInsertId();
			}
			else
			{
				$bindval = [];
				$bindkey = [];
				$q = "UPDATE page SET ";
				foreach($_POST as $key=>$val)
				{
					if ($key == 'page_id')continue;	// This is an autonumber.
					if (substr($key,0,4) == 'page')
					{
						$q.= "`$key`  = :".strtolower($key).",\n";
						$bindval[] = $val;
						$bindkey[] = ":$key";
					}
				}
				$q = substr($q,0,-2);
				$q.= " WHERE `page_id` = :page_id LIMIT 1";
				
				$db->query($q);

				for($i=0;$i<count($bindval);$i++)
				{
					$db->bind($bindkey[$i],$bindval[$i]);
				}
				$db->bind(":page_id",$_POST['page_id']);
				
				$x = $db->execute();
				$c = $db->rowCount();
			}
			break;
			case "media":
			
			if (!empty($_POST['action']))
			{
				$db = new Database();
				require WEBROOT.'/mvc/controllers/mediaController.php';
				switch($_POST['action'])
				{
					case "delete":	
					$db -> query("DELETE FROM media WHERE media_filename = :media LIMIT 1");
					$db -> bind(':media',\Utils::EndExplode("/",$_POST['media']));
					$db -> execute();

					@unlink(WEBROOT.'/uploads/'.\Utils::EndExplode("/",$_POST['media']));
					break;
					
					case "getDocuments":
						$MC = new MediaController();
						echo $MC->List($MC->GetDocuments());
						exit();
					break;
					
					case "getRecent":
						$MC = new MediaController();
						echo $MC->List($MC->GetRecent());
						exit();
					break;
					
					case "getMusic":
						$MC = new MediaController();
						echo $MC->List($MC->GetMusic());
						exit();
					break;
					
					case "getVideo":
						$MC = new MediaController();
						echo $MC->List($MC->GetVideos());
						exit();
					break;
					case "getImages":
						$MC = new MediaController();
						echo $MC->List($MC->GetImages());
						exit();
					break;
					case "getmeta":

						$media = new MediaController();
						die($media->GetMedia(\Utils::EndExplode("/",$_POST['media_filename'])));
						
					break;
					case "savemeta":
					case "addmeta":

						$db  = new Database();

						$q = [];
						$p = [];
						
						foreach($_POST as $key=>$val)
						{
							if (substr($key,0,5) == 'media')
							{
								$q[] = "`$key`";
								$p[] = ":".strtolower($key);
							}
						}
						$fields = implode(",",$q);
						$values = implode(",",$p);					
						
						$db->query('REPLACE INTO media ('.$fields.') VALUES ('.$values.')');
						
						for($i=0;$i<count($q);$i++)
						{
							$db->bind($p[$i],$_POST[str_replace("`","",$q[$i])]);
						}
						$db->execute();						

						header("Content-Type:application/json");
						die(json_encode(['message'=>'success']));						
						
					break;
				}
			}			
		}
		header("Content-Type:application/json");
		die(json_encode(['message'=>'success']));	
	}
}