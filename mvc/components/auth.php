<?php
/**
 * Authentication Module for role based management.
 * 
*/
class Auth extends X2Controller
{
	public  $user;		// Array [login,email,ip,active,status]
	private $id;		// DB id.
	private $pass;		// Password (never plaintext)
	public  $status;	// Logged in? Or not
	public  $role;		// Array [role_id,role_text,role_rules]
	public  $duration;	// How many seconds since last activity
	public function __construct()
	{
		$this->role = false;
		
	}
}