<?php
/**
 * X2 template. Allowed tokens:
 * 
 * SQL_TIMESTAMP	// Generally (but not always) date('Y-m-d H:i:s')
 * NOW()			// As above.
 * USER_ID			// Integer value corresponding to DB autonumber.
 * REMOTE_ADDR		// IP of user submitting form.
 * REMOTE_HOST		// Might not display anything at all.
 * HTTP_REFERER		// Last webpage user was on.
 * GEOIP_REGION		// Region determined by GeoIP.
 * GEOIP_COUNTRY	// Country determined by GeoIP.
 * GEOIP_CITY		// City determined by GeoIP (probably not very accurate).
 * LOGIN_METHOD		// Logged in using G+, FB, Native, etc
 * CURRENT_ROLE		// E.g. Admin, Root, Guest, Authenticated
*/
$salt  = '72684354irhfjsgy';	// Pull this from user table when generating a user.
$rules = [
	
		'random' => hash('sha256',time()),
	];

class FormModel
{
	private $_csrf;	// Will be inherited from parent: Stick here for now.
	private $_elmt;	// Whatever post element we're processing.
	private $rules;
	public function __construct($rules)
	{
		$this->rules		= $rules;
		$this->_csrf		= hash('ripemd160','X2'.rand(0,1000),false);	// Of course we'll make this stronger.
		$_SESSION['_csrf']	= $this->_csrf;
	}
	public function Process($what)
	{
		foreach($_POST as $key=>$val)
		{
			foreach($this->rules as $field=>$test)
			{
				if ($field == $key)
				{
					$_POST[$key] = $test;
					//if (X2_VERBOSE)echo($key." is now changed to $test.");
				}
			}
		}
		$this->verify($_POST['user_name'],$_POST['user_password']);
		return true;
	}
	private function Verify($username,$password)
	{
		$db = new \Database();
		$db->query("SELECT * FROM user WHERE user_name = :name LIMIT 1");
		$db->bind(":name",$username);
		$user = $db->single();
		if (empty($user))
		{
			$this->FailedLogin(-1,'Incorrect username or password','invalid_username');
		}
		$salt = $user['user_salt'];
		$db->query("SELECT * FROM user WHERE user_name = :name AND user_password = :password LIMIT 1");
		$db->bind(':name',$username);
		$db->bind(':password',hash('sha256',$password.$salt));
		$auth = $db->single();
		if (empty($auth))
		{
			$this->FailedLogin($user['user_id'],'Incorrect username or password','invalid_password');
		}
		$this->SuccessfulLogin($auth);
		exit();
		
	}
	private function FailedLogin($id,$friendly_msg,$status_msg)
	{
		unset($_SESSION['user']);
		$_SESSION['priv'] = $_SERVER['REMOTE_ADDR'];
		if ($status_msg == 'invalid_password')
		{
			/**
			 * Add failed_logins++
			*/
			$db = new \Database();
			$db->query("UPDATE user SET user_failed_logins = user_failed_logins+1 WHERE user_id = :id");
			$db->bind(':id',$id);
			$db->execute();
		}
		header("Content-Type:application/json");
		die(json_encode([
			
			'status'  => 'failed',
			'message' => $friendly_msg,
		]));
	}
	private function SuccessfulLogin($user)
	{		
		$_SESSION['priv'] = $user['user_is_root'] == 1?'root':($user['user_is_admin']?'admin':'user');
		$_SESSION['user'] = $user;
		header("Content-Type:application/json");
		die(json_encode([
			
			'status'  => 'success',
			'message' => 'Successfully logged in.',
		]));
	}

}
$Model = new FormModel($rules);