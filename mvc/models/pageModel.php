<?php
/**
 * X2 template. Allowed tokens:
 * 
 * SQL_TIMESTAMP	// Generally (but not always) date('Y-m-d H:i:s')
 * NOW()			// As above.
 * USER_ID			// Integer value corresponding to DB autonumber.
 * REMOTE_ADDR		// IP of user submitting form.
 * REMOTE_HOST		// Might not display anything at all.
 * HTTP_REFERER		// Last webpage user was on.
 * GEOIP_REGION		// Region determined by GeoIP.
 * GEOIP_COUNTRY	// Country determined by GeoIP.
 * GEOIP_CITY		// City determined by GeoIP (probably not very accurate).
 * LOGIN_METHOD		// Logged in using G+, FB, Native, etc
 * CURRENT_ROLE		// E.g. Admin, Root, Guest, Authenticated
*/

$rules = [
	
		'page_publish_date' => date(SQL_DATEFORMAT,strtotime(isset($_POST['page_publish_date'])?$_POST['page_publish_date']:'')),
	
	];



class PageModel
{
	private $_csrf;	// Will be inherited from parent: Stick here for now.
	private $_elmt;	// Whatever post element we're processing.
	private $rules;
	public function __construct($rules)
	{
		$this->rules		= $rules;
		$this->_csrf		= hash('ripemd160','X2'.rand(0,1000),false);	// Of course we'll make this stronger.
		$_SESSION['_csrf']	= $this->_csrf;
	}
	public function Process($what)
	{
		//print_r($this->rules);
		//echo($this->_csrf);
		foreach($_POST as $key=>$val)
		{
			foreach($this->rules as $field=>$test)
			{
				if ($field == $key)
				{
					$_POST[$key] = $test;
					//if (X2_VERBOSE)echo($key." is now changed to $test.");
				}
			}
		}
		return true;
	}
}

$Model = new PageModel($rules);